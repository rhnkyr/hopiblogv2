var mySwiper;
var specialContent = {
  swpInit:function(){
    mySwiper = new Swiper ('.swiper-container', {
      // Optional parameters
      direction: 'vertical',
      loop: false,
      noSwiping: 'swiper-slider__container'
    }) 
  },
  slideSwipers:function(){
    $('.swiper-slider__button').click(function(){
      $(this).parent().parent().parent().addClass('swiper--active').parent().siblings().children('.swiper-slider__container').removeClass('swiper--active');
      mySwiper.disableTouchControl();
    });
    $('.swiper-slider-button__return').click(function(){
      $(this).parent().parent().parent().removeClass('swiper--active');
      mySwiper.enableTouchControl();
    });
    $('.swiper-discover').click(function(){
      mySwiper.slideTo(1, 1000);
    });
  },
  init: function(){
    specialContent.swpInit();
    specialContent.slideSwipers();
  }
}

$(function(){
  specialContent.init()
});