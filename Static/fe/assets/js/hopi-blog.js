/* global $ */
var w, h;
var button = $('.load-more-button');
var buttonCircle = $('.load-more-button__container');
var buttonText = $('.load-more-button__text');
var buttonSpinner = $('.load-more-button__spinner');
var newsletterWidth;
var isMobile = {
  Android: function() {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function() {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function() {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function() {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function() {
    return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
  },
  any: function() {
    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
  }
};

if (document.readyState == 'complete') {
  removeLoading();
}

$(window).on('load', windowLoadEvent);

function windowLoadEvent() {
  removeLoading();
}

function removeLoading() {
  $('.loading').addClass('loaded');
  setTimeout(function () {
    $('.loading').remove();
  }, 1000);
}


var project = {
  resizeEvents: function() {
    $(window).resize(function() {
      w = $(this).width();
      h = $(this).height();
      if(w > 576){
        newsletterWidth = 66+'px';
      }else {
        newsletterWidth = 45+'px';
      }
    }).trigger('resize')
  },
  removeOverflow:function(){
    $('body').removeClass('overflow-hidden');
  },
  openOtherDropdown:function(){
    $('.has--dropdown > a').click(function(e){
      e.stopPropagation();
      if(isMobile.any()){
         $('.mobile-dropdown').addClass('mobile-dropdown--active')
         $('html, body').addClass('overflow-hidden');
      }else {
        $('.header-dropdown').slideToggle();
        $(this).toggleClass('active');
      }
    });
    $('.header-dropdown__mobile-button').click(function(){
      $('.mobile-dropdown').removeClass('mobile-dropdown--active')
      $('html, body').removeClass('overflow-hidden');
    });
  },
  openCloseSearch:function(){
    $('.top-search__slide-button').click(function(){
      $('.top-search').addClass('active');
      setTimeout(function(){
        $('.top-search__submit').fadeIn();
      },500)
      $('.top-search__input').focus();
    });
    $(document).click(function(event) {
      if (event.target.className != "icon-search" && event.target.className != "icon-search:before" && event.target.className != "top-search" && event.target.className != "top-search__slide-button" && event.target.className != "top-search__input" && event.target.className != "top-search__submit") {
          $('.top-search__submit').fadeOut(100,function(){
            $('.top-search').removeClass('active');   
          });
      }
      // Diger menusu
      if (event.target.className != "has--dropdown" && event.target.className != "header-dropdown" && event.target.className != "dropdown__item") {
          $('.has--dropdown > a').removeClass('active');
          $('.header-dropdown').slideUp();
      }
    });
    // for mobile
    $('.top-search__mobile-button').click(function(){
      $('.search-fixed-module').addClass('search-fixed-module--active');
      $('.search-fixed-module__input').focus();
    });
    $('.search-fixed-module__close').click(function(){
      $('.search-fixed-module').removeClass('search-fixed-module--active');
    });
  },
  initCarousels: function() {
    if (typeof $.fn.owlCarousel === "function") {
      $('#main-carousel.owl-carousel').owlCarousel({
        margin: 0,
        padding: 0,
        smartSpeed: 1500,
        loop: true,
        responsive: {
          0: {
            items: 1,
            margin: 30,
            autoWidth: true,
            stagePadding: 50,
            nav: false,
            dots: false
          },
          768: {
            items: 1,
            stagePadding: 0,
            autoWidth: false,
            margin: 0,
            nav: false,
            dots: true
          },
          992: {
            items: 1,
            stagePadding: 0,
            autoWidth: false,
            margin: 0,
            nav: true,
            dots: true
          }
        }
      });
      $('#card-carousel.owl-carousel').owlCarousel({
        responsive: {
          0: {
            margin: 20,
            padding: 20,
            autoWidth: true,
            stagePadding: 50,
            loop: true,
            items: 1
          }
        }
      });
      $('.top-menu-mobile div.owl-carousel').owlCarousel();
      $('#gallery-carousel.owl-carousel').owlCarousel({
        items: 1,
        margin: 0,
        padding: 0,
        dots: true,
        loop: true,
        responsive: {
          0: {
            nav: false
          },
          992: {
            nav: true,
            smartSpeed: 1000,
            animateOut: 'slideOutLeft',
            animateIn: 'slideInRight'
          }
        }
      });
    }
  },
  initFancybox:function(){
    if(typeof $.fn.fancybox === "function"){
      $(".photo-gallery-mask").fancybox({
    		openEffect	: 'none',
    		closeEffect	: 'none',
    		padding: 0,
    		wrapCSS: 'gallery-fancybox'
    	});
    }
  },
  initVideoPlay:function(){
    $('.play-button').click(function(){
      var videoURL = $(this).attr('rel');
      var temp = '<iframe id="#generalVideo" class="video-plugin" src="'+ videoURL +'" width="100%" height="650" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
      $('.page-image').append(temp);
      $('.video-image-holder').fadeOut();
      $('.page-image__text-holder h1').fadeOut();
    });
  },
  clearInput:function(){
    $('.clear-input').click(function(){
      $(this).prev().val("");
      $(this).prev().focus()
    })
  },
  loadMoreButton:function(){
    button.click(function() {
    	startSpinner();
    	
    	setTimeout(function() {
    		stopSpinner();
    	}, 5000);
    });
  },
  readMoreButton:function() {
    $('.read-more-module').click(function() {
       $(this).hide();
       $('.mobile-wrapped-content').show();
    });
  },
  sendNewsletterForm:function() {
    // ajax here
    $.post('/abonelik-kayit',{email : $(".newsletter__input").val()},function () {
        setTimeout(function(){
            $('#tick').css({ opacity: 1 }).addClass("drawn");
            $('#newsletterMessage').addClass("fadeIn animated");
            setTimeout(function() {
                $('.newsletter__holder').removeClass('newsletter__button--is-loading newsletter-is-loading-spinner');
                //.removeAttr('style').css({ opacity: 0 });
            }, 500);

        }, 3000); 
    },'json');
  },
  newsletter:function() {
    $('.newsletter__holder form').submit(function(e) {
      e.preventDefault();
        $('.newsletter__holder form').fadeOut(500, function() {
        
          
          $('.newsletter__holder')
            .css({ overflow: 'hidden' })
            .stop()
            .animate(
              { 
                width: newsletterWidth, 
                borderWidth: '1px'
              },
              { 
                easing: 'easeInOutCirc', 
                duration: 750, 
                complete: function() {
                  
                  $('.newsletter__holder').addClass('newsletter__button--is-loading newsletter-is-loading newsletter-is-loading-spinner');
                  
                  project.sendNewsletterForm();
                  
                  
                }
                
              }
            );
        
        });
    });
    
  },
  facebookShare: function(n) {
      var t, i, r = $(".page-image__text-holder h1").text(), u = $(".page-image img").attr("src");
      t = window.location.href + "?utm_source=facebook.com&utm_medium=referral&utm_campaign=kullanici-paylasimi";
      i = "https://www.facebook.com/share.php?u=" + encodeURIComponent(t) + "&title=" + r;
      window.open(i, "Facebook", "height=269,width=550,resizable=1");
  },
  twitterShare:function(){
    var t, i, r = $(".page-image__text-holder h1").text(), u = $(".page-image img").attr("src");
    t = window.location.href + "?utm_source=t.co&utm_medium=referral&utm_campaign=kullanici-paylasimi";
    i = "https://twitter.com/intent/tweet?url=" + encodeURIComponent(t) + "&text=" + r;
    window.open(i, "Twitter", "height=285,width=550,resizable=1");
  },
  init: function() {
    project.resizeEvents();
    project.removeOverflow();
    project.openCloseSearch();
    project.openOtherDropdown();
    project.initFancybox();
    project.initVideoPlay();
    project.clearInput();
    project.loadMoreButton();
    project.readMoreButton();
    project.newsletter();
  }
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function startSpinner() {
	button.addClass('load-more-button--is-loading');
	buttonText.animate({ opacity: 0 }, {duration: 150, easing: 'easeInOutCirc'});
	buttonCircle.animate({ width: '36px', left: '58px' }, {duration: 500, easing: 'easeInOutCirc', complete: function() {
		//buttonCircle.css({ opacity: 0 });
		buttonSpinner.css({ opacity: 1 });
	}});
}

function stopSpinner() {
	button.removeClass('load-more-button--is-loading');
	buttonSpinner.css({ opacity: 0 });
	//buttonCircle.css({ opacity: 1 });
	buttonText.stop().delay(300).animate({ opacity: 1 }, {duration: 200, easing: 'easeInOutCirc'});
	buttonCircle.stop().animate({ width: '152px', left: '0' }, {duration: 500, easing: 'easeInOutCirc'});
}

function bindFormValidation() {

  // mask first
  $('input[data-validation-mask]').each(function(i, el) {
    var curr = $(el);
    curr.mask(curr.attr('data-validation-mask'));
  });

  // clear on focus
  $('input[data-validate="true"], textarea[data-validate="true"]').focus(function() {
    $(this).closest('.form-item__holder').removeClass('item--error');
  });

  $('#message').keypress(function(e) {
    var tval = $('textarea').val(),
      tlength = tval.length,
      set = 1000,
      remain = parseInt(set - tlength);
    $('.counter span').text(remain);
    if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
      $('#message').val((tval).substring(0, tlength - 1))
    }
  });

  $('form').submit(function(e) {
    e.preventDefault();

    if (validateForm()) {
      $('form').submit();
    }
  });
}

function validateForm() {
  var isValid = 1;

  $('input[data-validate="true"], textarea[data-validate="true"]').each(function(i, el) {
    var curr = $(el);
    var validationType = curr.attr('data-validation-type') ? curr.attr('data-validation-type') : "";

    if (validationType === "email" && !validateEmail(curr.val())) {
      isValid = 0;
      curr.closest('.form-item__holder').addClass('item--error');
    }

    if (curr.val() == "") {
      isValid = 0;
      curr.closest('.form-item__holder').addClass('item--error');
    }

  });

  return isValid;
}

function createCookie(name, value, days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    var expires = "; expires=" + date.toGMTString();
  } else var expires = "";
  document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

function eraseCookie(name) {
  createCookie(name, "", -1);
}

function init() {
    project.initCarousels();
    project.init();
}

$(function() {
  project.initCarousels();
  project.init();
});