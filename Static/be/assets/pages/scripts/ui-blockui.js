var UIBlockUI = function() {

    var handle = function() {

        $('#blockui_sample_1_1').click(function() {
            App.blockUI({
                target: '#blockui_sample_1_portlet_body'
            });

            window.setTimeout(function() {
                App.unblockUI('#blockui_sample_1_portlet_body');
            }, 2000);
        });

        $('#blockui_sample_1_2').click(function() {
            App.blockUI({
                target: '#blockui_sample_1_portlet_body',
                boxed: true
            });

            window.setTimeout(function() {
                App.unblockUI('#blockui_sample_1_portlet_body');
            }, 2000);
        });

        $('#blockui_sample_1_3').click(function() {
            App.blockUI({
                target: '#blockui_sample_1_portlet_body',
                animate: true
            });

            window.setTimeout(function() {
                App.unblockUI('#blockui_sample_1_portlet_body');
            }, 2000);
        });
    };
    
    return {
        //main function to initiate the module
        init: function() {
            handleSample1();
        }
    };

}();

jQuery(document).ready(function() {    
   UIBlockUI.init();
});