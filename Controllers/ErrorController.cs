﻿using System.Web.Mvc;
using HopiBlog.Helpers;

namespace HopiBlog.Controllers
{
    public class ErrorController : Controller
    {
        [HttpGet]
        public ActionResult NotFound()
        {
            if (Request.Url == null) return View("Error");
            var urlToTest = Request.Url.AbsolutePath.Trim('/');
            var redirect = RedirectManager.GetRedirectFrom(urlToTest);
            if (redirect != null)
            {
                return redirect.RedirectType == "301" ? RedirectPermanent(redirect.NewUrl) : Redirect(redirect.NewUrl);
            }
            Response.StatusCode = 404;
            ViewBag.UrlToTest = urlToTest;
            return View("Error");
        }
    }
}
