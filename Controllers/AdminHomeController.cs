﻿using System.Web.Mvc;
using HopiBlog.Helpers;
using HopiBlog.Repository;

namespace HopiBlog.Controllers
{
    public class AdminHomeController : Controller
    {
        private readonly AdminRepository _ar = new AdminRepository(Utilities.ConnectionString("HopiBlogConnection"));
        private const string PasswordAdditional = "FtqxZ4zq7nyw"; 
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                ViewBag.Status = "Kullanıcı adı ve şifre giriniz!";
            else
            {
                var memberItem = _ar.CheckAdmin(username, Utilities.Sha1(PasswordAdditional +""+ password));
                if (memberItem != null)
                {
                    AdminProperties.CurrentMember = memberItem;
                    return RedirectToAction("Dashboard", "Backoffice");
                }
                ViewBag.Status = "Hatalı kullanıcı adı veya şifre";
            }
            return View();
        }
    }
}
