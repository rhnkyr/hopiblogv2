﻿using System.Web.Mvc;
using HopiBlog.Helpers;

namespace HopiBlog.Controllers
{
    [AdminAuthHelper.RoleAuthorization]
    public class AdminBaseController : Controller
    {
    }
}
