﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using HopiBlog.Helpers;
using HopiBlog.Models;
using HopiBlog.Repository;
using HopiBlog.ViewModels.Site;
using Newtonsoft.Json;
using WebMarkupMin.AspNet4.Mvc;

namespace HopiBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly SiteRepository _sr =
            new SiteRepository(Utilities.ConnectionString("HopiBlogConnection"));

        private readonly int _listerCount = Convert.ToInt32(Utilities.AppSetting("ListerCount"));

        public HomeController()
        {
            var routeValues = System.Web.HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("slug"))
                ViewData["MenuElement"] = (string) routeValues["slug"];
            else if (System.Web.HttpContext.Current.Request.QueryString.AllKeys.Contains("slug"))
                ViewData["MenuElement"] = System.Web.HttpContext.Current.Request.QueryString["slug"];
            else
                ViewData["MenuElement"] = "";

            var mainLayoutViewModel = new MainLayoutViewModel
            {
                TopCategories = _sr.GetCategories(0),
                SubCategories = _sr.GetCategories(1),
                Setting       = _sr.Settings()
            };
            ViewData["MainLayoutViewModel"] = mainLayoutViewModel;
            ViewData["Mobile"]              = Utilities.BrowserIsMobile() ? "1" : "0";
            ViewData["Counter"]             = _listerCount;
        }

        [HttpPost]
        public string LoadMore(int i)
        {
            var lister = i * _listerCount;
            var mul    = i * _listerCount + 1;
            var data   = _sr.LatestPosts(mul, lister + _listerCount);
            return JsonConvert.SerializeObject(data);
        }

        /*[HttpPost]
        public string LoadMoreTags(int i)
        {
            var lister = i * _listerCount;
            var mul = i * _listerCount + 1;
            var data = _sr.GetTagContent(mul, lister + _listerCount);
            return JsonConvert.SerializeObject(data);
        }*/

        [HttpGet]
        [CompressContent]
        [MinifyHtml]
        //[OutputCache(CacheProfile = "CacheCompressedContent")]
        public ActionResult Index()
        {
            
            var hpvm = new HomePageViewModel
            {
                Sliders     = _sr.Sliders(),
                LatestPosts = _sr.LatestPosts(0, _listerCount),
                MostRead    = _sr.MostRead(9),
                TotalPost = _sr.TotalPost()
            };
            return View(hpvm);
        }

        [HttpGet]
        [CompressContent]
        [MinifyHtml]
        //[OutputCache(CacheProfile = "CacheCompressedContent")]
        public ActionResult ShowTagContent(string slug)
        {
            var posts = _sr.GetTagContent(8, slug);

            if (!posts.Any())
            {
                return Redirect("/");
            }

            var lcmv = new ContentListViewModel
            {
                Tag   = _sr.GetTagName(slug),
                Count = _sr.CountPosts(),
                Post  = posts
            };

            return View("ListContent", lcmv);
        }

        [HttpGet]
        [CompressContent]
        [MinifyHtml]
        //[OutputCache(CacheProfile = "CacheCompressedContent")]
        public ActionResult ShowCategoryContent(string slug)
        {
            var posts = _sr.GetCategoryContent(8, slug);

            if (!posts.Any())
            {
                return Redirect("/");
            }

            var lcmv = new ContentListViewModel
            {
                Category = _sr.GetCategoryName(slug),
                Count    = _sr.CountPosts(),
                Post     = _sr.GetCategoryContent(8, slug)
            };

            return View("ListContent", lcmv);
        }

        /// <summary>
        /// İçerik sluga göre tür belirleme ve render
        /// </summary>
        /// <param name="slug"></param>
        /// <returns></returns>
        [HttpGet]
        [CompressContent]
        [MinifyHtml]
        //[OutputCache(CacheProfile = "CacheCompressedContent")]
        public ActionResult ShowContent(string slug)
        {
            var post = _sr.GetPost(slug);

            if (post == null) return Redirect("/");

            if (post.PostStatus == 2 && AdminProperties.CurrentMember == null) return Redirect("/");

            var tags = _sr.GetPostTags(post.Id);

            var relatedPosts = _sr.GetRelatedPosts(post.Id, post.CatSlug);


            ListDetailViewsModel    lw  = null;
            SitePostViewModel       pw  = null;
            SpecialContentViewModel scw = null;

            if (post.PostType < 3)
            {
                pw = new SitePostViewModel
                {
                    Post         = post,
                    CategoryName = _sr.GetPostCategory(post.CatSlug),
                    Tags         = tags,
                    RelatedPosts = relatedPosts
                };
            }
            else if (post.PostType == 3)
            {
                pw = new SitePostViewModel
                {
                    Post         = post,
                    CategoryName = _sr.GetPostCategory(post.CatSlug),
                    Tags         = tags,
                    RelatedPosts = relatedPosts,
                    PostImages   = _sr.GetPostImages(post.Id)
                };
            }
            else if (post.PostType == 6)
            {
                scw = new SpecialContentViewModel
                {
                    Post         = post,
                    Tags         = tags,
                    RelatedPosts = relatedPosts,
                    IsMobile     = Utilities.BrowserIsMobile()
                };
            }
            else
            {
                lw = new ListDetailViewsModel
                {
                    Post         = post,
                    CategoryName = _sr.GetPostCategory(post.CatSlug),
                    ListElements = _sr.GetListElements(post.Id),
                    Tags         = tags,
                    RelatedPosts = relatedPosts
                };
            }

            switch (post.PostType)
            {
                case (byte) PostType.PlainText:
                    return View("PlainPost", pw);
                case (byte) PostType.Image:
                    return View("ImagePost", pw);
                case (byte) PostType.Video:
                    return View("VideoPost", pw);
                case (byte) PostType.List:
                    return View("ListPost", lw);
                case (byte) PostType.ShopList:
                    return View("ShopListPost", lw);
                case (byte) PostType.SpecialContent:
                    return View(Utilities.BrowserIsMobile() ? "SpecialContentMobilePost" : "SpecialContentWebPost",
                        scw);
            }

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Admin kullanıcısı için eklediği içereği kontrol etmesini sağlar
        /// </summary>
        /// <param name="slug"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Preview(string slug)
        {
            if (AdminProperties.CurrentMember == null) return RedirectToAction("Index", "Home");

            var post = _sr.GetPost(slug);

            if (post == null) return Redirect("/");

            var tags = _sr.GetPostTags(post.Id);

            var relatedPosts = _sr.GetRelatedPosts(post.Id, post.CatSlug);

            var pw = new SitePostViewModel
            {
                Post         = post,
                CategoryName = _sr.GetPostCategory(post.CatSlug),
                Tags         = tags,
                RelatedPosts = relatedPosts
            };

            switch (post.PostType)
            {
                case (byte) PostType.PlainText:
                    return View("PlainPost", pw);

                case (byte) PostType.Image:
                    var gpw = new SitePostViewModel
                    {
                        Post         = post,
                        CategoryName = _sr.GetPostCategory(post.CatSlug),
                        PostImages   = _sr.GetPostImages(post.Id),
                        Tags         = tags,
                        RelatedPosts = relatedPosts
                    };
                    return View("ImagePost", gpw);

                case (byte) PostType.Video:
                    return View("VideoPost", pw);

                case (byte) PostType.List:
                    var ldw = new ListDetailViewsModel
                    {
                        Post         = post,
                        ListElements = _sr.GetListElements(post.Id),
                        Tags         = tags,
                        RelatedPosts = relatedPosts
                    };
                    return View("ListPost", ldw);
            }

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Email abonelik kayıt
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveEmail(string email)
        {
            var checkSubscription = _sr.CheckUserSubscription(email);

            if (checkSubscription != null) return Json(new {status = false}, JsonRequestBehavior.AllowGet);

            var sub = new Subscription
            {
                Email       = email,
                CreatedDate = DateTime.Now
            };
            var ret                 = _sr.SaveEmail(sub);
            return Json(new {status = ret}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search()
        {
            var param = Request.QueryString["kelime"];

            ViewBag.Word = param;

            if (string.IsNullOrEmpty(param)) return Redirect("/");

            var contents = _sr.GetSearchResults(param);

            return View("SearchResult", contents);
        }

        [HttpGet]
        public void SiteMap()
        {
            var domain = Utilities.AppSetting("SiteMainUrl");

            Robot.ParseRobotsTxtFile(domain);

            var sitemap = new Sitemap();

            sitemap.Add(new Location
            {
                Url          = Utilities.AppSetting("SiteMainUrl"),
                LastModified = DateTime.UtcNow.AddDays(-1)
            });

            var siteMapDataPages = _sr.GetSiteMap();

            //Category
            foreach (var sm in siteMapDataPages.Where(s => s.Type == (int) SiteMapType.Category).OrderBy(s => s.Url))
            {
                var actual = Robot.URLIsAllowed(domain + sm.Url);
                if (actual)
                {
                    sitemap.Add(new Location
                    {
                        Url             = domain + sm.Url,
                        ChangeFrequency = sm.ChangeFreq,
                        Priority        = sm.Priority,
                        LastModified    = sm.LastModified
                    });
                }
            }

            //Subpage
            foreach (var sm in siteMapDataPages.Where(s => s.Type == (int) SiteMapType.Subpage))
            {
                var actual = Robot.URLIsAllowed(domain + sm.Url);
                if (actual)
                {
                    sitemap.Add(new Location
                    {
                        Url             = domain + sm.Url,
                        ChangeFrequency = sm.ChangeFreq,
                        Priority        = sm.Priority,
                        LastModified    = sm.LastModified
                    });
                }
            }

            Response.Clear();
            var xs               = new XmlSerializer(typeof(Sitemap));
            Response.ContentType = "text/xml";
            xs.Serialize(Response.Output, sitemap);
            Response.End();
        }
    }

    public class MainLayoutViewModel
    {
        public IEnumerable<Category> TopCategories { get; set; }
        public IEnumerable<Category> SubCategories { get; set; }
        public SiteSetting           Setting       { get; set; }
    }


    //todo : dapper sql IN
    /*string sql = "SELECT * FROM SomeTable WHERE id IN @ids"
var results = conn.Query(sql, new { ids = new[] { 1, 2, 3, 4, 5 });*/
}
