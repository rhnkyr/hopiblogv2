﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using ClosedXML.Excel;
using HopiBlog.Helpers;
using HopiBlog.Models;
using HopiBlog.Repository;
using HopiBlog.ViewModels.Admin;
using Kaliko.ImageLibrary;
using Kaliko.ImageLibrary.Scaling;
using NLog;

namespace HopiBlog.Controllers
{
    public class BackofficeController : AdminBaseController
    {
        private readonly AdminRepository _ar = new AdminRepository(Utilities.ConnectionString("HopiBlogConnection"));

        private readonly string _perPage = Utilities.AppSetting("GridPageSize");

        private const string PasswordAdditional = "FtqxZ4zq7nyw";

               
        /// <summary>
        /// Güncel Sayfası
        /// </summary>
        /// <returns></returns>
        public ActionResult Dashboard()
        {
            var dbwm = new DashBoardViewModel
            {
                Posts = _ar.GetAllPosts()
            };
            return View(dbwm);
        }

        //todo : sitemap ekle eksik
        //todo : special content işlemleri
        //sitemap yönetimi

        #region SiteMap

        public ActionResult Robots()
        {
            var path = Server.MapPath(@"~/robots.txt");

            using (var fileStream = System.IO.File.OpenRead(path))
            using (var streamReader = new StreamReader(fileStream))
            {
                var fileContent = streamReader.ReadToEnd();

                ViewBag.Robots = fileContent;
            }

            return View("SiteMap/RobotTxt");
        }

        [HttpPost]
        public ActionResult UpdateRobots()
        {
            var path = Server.MapPath(@"~/robots.txt");

            System.IO.File.WriteAllText(path, Request.Form["rcode"]);

            TempData["Flash"] = true;
            return RedirectToAction("Robots", "Backoffice");
        }

        [HttpPost]
        public JsonResult UpdateSiteMapValue(int i, string v, string w)
        {

            _ar.UpdateSiteMapValue(i, v, w);
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SiteMapList()
        {
            var siteMapDataPages = _ar.GetSiteMap();
            return View("SiteMap/SiteMapList", siteMapDataPages);
        }

        #endregion

        #region Category

        public ActionResult Category(int id = 0)
        {
            var category = _ar.GetCategory(id);
            return View("Category/AddCategory", category);
        }

        [HttpPost]
        public ActionResult AddCategory()
        {
            var category = new Category
            {
                CatName = Request.Form["name"],
                CatSlug = Utilities.ToSlug(Request.Form["name"])
            };
            var cid = _ar.AddCategory(category);

            if (cid != null)
            {
                var sitemap = new Models.SiteMap
                {
                    PostId = (int) cid,
                    Type = (int) SiteMapType.Category,
                    Url = Url.RouteUrl("ShowPlainAndSpecialContent",
                        new {slug = Utilities.ToSlug(Request.Form["name"])}),
                    LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                    ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                };

                _ar.AddSiteMap(sitemap);
            }

            TempData["Flash"] = true;

            return RedirectToAction("ListCategories", "Backoffice");
        }

        [HttpPost]
        public ActionResult UpdateCategory()
        {
            var category = new Category
            {
                CatName = Request.Form["name"],
                CatSlug = Utilities.ToSlug(Request.Form["name"])
            };

            var cid = Convert.ToInt32(Request.Form["cid"]);

            _ar.UpdateCategory(cid, category);

            var sitemap = new Models.SiteMap
            {
                PostId = cid,
                Type = (int) SiteMapType.Category,
                Url = Url.RouteUrl("ShowPlainAndSpecialContent", new {slug = Utilities.ToSlug(Request.Form["name"])}),
                LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
            };

            _ar.UpdateSiteMap(cid, (int) SiteMapType.Category, sitemap);

            TempData["Flash"] = true;

            return RedirectToAction("ListCategories", "Backoffice");
        }

        [HttpPost]
        public JsonResult DeleteCategory(int id)
        {
            _ar.DeleteCategory(id);

            _ar.DeleteSiteMapData(id, (int) SiteMapType.Category);

            TempData["Flash"] = true;
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListCategories()
        {
            var categories = _ar.ListCategories().OrderBy(c => c.CatName);
            return View("Category/CategoryList", categories);
        }

        #endregion

        #region Sliders

        public ActionResult Slider(int id = 0)
        {
            var slider = _ar.GetSlider(id);

            return View("Slider/AddSlider", slider);
        }

        [HttpPost]
        public JsonResult DeleteSlider(int id)
        {
            _ar.DeleteSlider(id);
            TempData["Flash"] = true;
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteSliders(int[] values)
        {
            foreach (var id in values)
            {
                _ar.DeleteSlider(id);
            }

            TempData["Flash"] = true;
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OrderSlider()
        {
            var data = Request.Form["slider-table[]"].Split(',').Select(int.Parse).ToArray();
            var listingCounter = 1;

            foreach (var id in data)
            {
                var slider = _ar.GetSlider(id);
                if (slider == null) continue;
                slider.Ord = listingCounter;
                _ar.UpdateSliderOrder(id, slider);
                listingCounter++;
            }

            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddSlider(HttpPostedFileBase image)
        {
            if (image != null && image.ContentLength > 0)
            {
                var newName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var newNameMobile = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                    Path.GetExtension(image.FileName);

                try
                {
                    var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");

                    var pathString = Path.Combine(originalDirectory.ToString(), "Temp");

                    var isExists = Directory.Exists(pathString);

                    if (!isExists)
                        Directory.CreateDirectory(pathString);

                    var path = $"{pathString}\\{newName}";

                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);

                    var pathMobile = $"{pathString}\\{newNameMobile}";

                    //375 x260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    Utilities.UploadToCdn(newName, path);
                    Utilities.UploadToCdn(newNameMobile, pathMobile);

                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                var slider = new Slider
                {
                    Image = newName,
                    ImageMobile = newNameMobile,
                    ImageName = Request.Form["imageName"],
                    ImageAltText = Request.Form["imageAltText"],
                    TopText = Request.Form["toptext"],
                    BottomText = Request.Form["subtext"],
                    Link = Request.Form["link"],
                    NoFollow = (byte) (string.IsNullOrEmpty(Request.Form["noflw"]) ? 1 : 0),
                    Ord = 0
                };
                _ar.AddSlider(slider);
            }
            else // Validation error, so redisplay same view
                return View("Slider/AddSlider");

            TempData["Flash"] = true;

            return RedirectToAction("ListSliders", "Backoffice");
        }

        [HttpPost]
        public ActionResult UpdateSlider(HttpPostedFileBase image)
        {
            if (image != null && image.ContentLength > 0)
            {
                var newName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var newNameMobile = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                    Path.GetExtension(image.FileName);
                try
                {
                    var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                    var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                    var isExists = Directory.Exists(pathString);
                    if (!isExists)
                        Directory.CreateDirectory(pathString);
                    var path = $"{pathString}\\{newName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{newNameMobile}";

                    //375 x260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);
                    Utilities.UploadToCdn(newName, path);
                    Utilities.UploadToCdn(newNameMobile, pathMobile);
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }
                var slider = new Slider
                {
                    Image = newName,
                    ImageMobile = newNameMobile,
                    ImageName = Request.Form["imageName"],
                    ImageAltText = Request.Form["imageAltText"],
                    TopText = Request.Form["toptext"],
                    BottomText = Request.Form["subtext"],
                    Link = Request.Form["link"],
                    NoFollow = (byte) (!string.IsNullOrEmpty(Request.Form["noflw"]) ? 1 : 0)
                };
                _ar.UpdateSlider(Convert.ToInt32(Request.Form["sid"]), slider);
            }
            else
            {
                var slider = new Slider
                {
                    Image = Request.Form["ex_image"],
                    ImageMobile = Request.Form["ex_image_mobile"],
                    ImageName = Request.Form["imageName"],
                    ImageAltText = Request.Form["imageAltText"],
                    TopText = Request.Form["toptext"],
                    BottomText = Request.Form["subtext"],
                    Link = Request.Form["link"],
                    NoFollow = (byte) (!string.IsNullOrEmpty(Request.Form["noflw"]) ? 1 : 0)
                };
                _ar.UpdateSlider(Convert.ToInt32(Request.Form["sid"]), slider);
            }

            TempData["Flash"] = true;

            return RedirectToAction("ListSliders", "Backoffice");
        }

        public ActionResult ListSliders()
        {
            var sliders = _ar.ListSliders();
            return View("Slider/SliderList", sliders.OrderBy(s => s.Ord));
        }

        #endregion

        #region CommonPost

        public JsonResult DeletePost(int id)
        {
            _ar.DeletePost(id);
            _ar.DeletePostTagReferences(id);
            _ar.DeletePostListElements(id);
            _ar.DeleteSiteMapData(id, (int) SiteMapType.Subpage);

            TempData["Flash"] = true;
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeletePosts(int[] values)
        {
            foreach (var id in values)
            {
                _ar.DeletePost(id);
                _ar.DeletePostTagReferences(id);
                _ar.DeletePostListElements(id);
                _ar.DeleteSiteMapData(id, (int) SiteMapType.Subpage);
            }

            TempData["Flash"] = true;
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetPostListBySearch(string key)
        {
            ViewBag.Search = "true";

            var lw = new ListViewModel
            {
                Posts = _ar.GetPostListByKey(key)
            };

            return View("Post/PostList", lw);
        }

        public ActionResult GetPostListByType(int type, int page = 1)
        {
            ViewBag.Page = "PostList";
            ViewBag.Type = type;
            switch (type)
            {
                case (byte) PostType.PlainText:
                    ViewBag.Title = "Düz Yazı İçerikler";
                    break;
                case (byte) PostType.Image:
                    ViewBag.Title = "Görsel İçerikler";
                    break;
                case (byte) PostType.Video:
                    ViewBag.Title = "Video İçerikler";
                    break;
                case (byte) PostType.List:
                    ViewBag.Title = "Liste İçerikler";
                    break;
                case (byte) PostType.ShopList:
                    ViewBag.Title = "Shop Liste İçerikler";
                    break;
            }
            var total = _ar.TotalPosts(type);
            var ps = Convert.ToInt32(_perPage);
            var begin = SetBegin(page, ps);
            var end = SetEnd(begin, page, ps);
            var lw = new ListViewModel
            {
                PageSize = ps,
                Posts = _ar.GetPostListByType(type, begin, end),
                PageNumber = page,
                TotalItemCount = total
            };
            return View("Post/PostList", lw);
        }

        public ActionResult PostDetail(int pid)
        {
            var post = _ar.GetPost(pid);
            if (post == null) return RedirectToAction("Dashboard");

            PostDetailViewModel pdvm;

            if (post.PostType < 3)
            {
                pdvm = new PostDetailViewModel
                {
                    Post = post,
                    Categories = _ar.GetCategories(),
                    Tags = _ar.GetPostTags(pid)
                };
            }
            else if (post.PostType == 3)
            {
                pdvm = new PostDetailViewModel
                {
                    Post = post,
                    Categories = _ar.GetCategories(),
                    Tags = _ar.GetPostTags(pid),
                    PostImages = _ar.GetPostImages(pid)
                };
            }
            else
            {
                pdvm = new PostDetailViewModel
                {
                    Post = post,
                    Categories = _ar.GetCategories(),
                    ListElements = _ar.GetListElements(pid),
                    Tags = _ar.GetPostTags(pid)
                };
            }
            switch (post.PostType)
            {
                case (byte) PostType.PlainText:
                    ViewBag.Title = "Düz Yazı İçerikler";
                    return View("Post/PlainPost", pdvm);
                case (byte) PostType.Image:
                    ViewBag.Title = "Görsel İçerikler";
                    return View("Post/ImagePost", pdvm);
                case (byte) PostType.Video:
                    ViewBag.Title = "Video İçerikler";
                    return View("Post/VideoPost", pdvm);
                case (byte) PostType.List:
                    ViewBag.Title = "Liste İçerikler";
                    return View("Post/ListedPost", pdvm);
                case (byte) PostType.ShopList:
                    ViewBag.Title = "Shop Liste İçerikler";
                    return View("Post/ListedShopPost", pdvm);
            }
            return RedirectToAction("Dashboard");
        }

        public ActionResult AddPost(int type)
        {
            ViewData["categories"] = _ar.GetCategories();

            switch (type)
            {
                case (byte) PostType.PlainText:
                    return View("Post/PlainPost");
                case (byte) PostType.Image:
                    return View("Post/ImagePost");
                case (byte) PostType.Video:
                    return View("Post/VideoPost");
                case (byte) PostType.List:
                    return View("Post/ListedPost");
                case (byte) PostType.ShopList:
                    return View("Post/ListedShopPost");
                default:
                    return RedirectToAction("Dashboard");
            }
        }

        /// <summary>
        /// Token input için tag json datası döndürür
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTags()
        {
            var data = _ar.GetTags(Request.QueryString["key"]);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PlainPost

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddPlainPost(FormCollection form, HttpPostedFileBase image)
        {
            //upload işlemleri

            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);

                try
                {
                    var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                    var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                    var isExists = Directory.Exists(pathString);
                    if (!isExists)
                        Directory.CreateDirectory(pathString);

                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x 260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
                
                var post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 1,
                    ReadCount = 0
                };

                var pid = _ar.AddPost(post);
                var tags = form["tags"].Split(',');
                foreach (var tag in tags)
                {
                    var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));
                    if (pid != null)
                    {
                        _ar.SavePostTagRefence(new PostTagReference
                        {
                            TagId = checkTag.Id,
                            PostId = (int) pid
                        });
                    }
                }

                if (pid != null && (byte) Convert.ToInt32(form["noflw"]) == 0)
                {
                    var sitemap = new Models.SiteMap
                    {
                        PostId = (int) pid,
                        Type = (int) SiteMapType.Subpage,
                        Url = Url.RouteUrl("ShowPlainAndSpecialContent", new {slug = post.PostSlug}),
                        LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                        ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                    };

                    _ar.AddSiteMap(sitemap);
                }
            }
            else
                return View("Post/PlainPost");


            TempData["Flash"] = true;

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 1});
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdatePlainPost(FormCollection form, HttpPostedFileBase image)
        {
            var pid = Convert.ToInt32(form["pid"]);
            var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
            Post post;

            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);
                try
                {
                    var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                    var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                    var isExists = Directory.Exists(pathString);
                    if (!isExists)
                        Directory.CreateDirectory(pathString);
                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

              
                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 1
                };
            }
            else
            {
                
                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = form["ex_image"],
                    ImageName = form["imageName"],
                    MobileImage = form["ex_image_mobile"],
                    ThumbImage = form["ex_image_thumb"],
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 1
                };
            }

            _ar.UpdatePost(pid, post);
            var tags = form["tags"].Split(',');

            //Var olan referansları sil
            _ar.DeletePostTagReferences(pid);
            foreach (var tag in tags)
            {
                var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                _ar.SavePostTagRefence(new PostTagReference
                {
                    TagId = checkTag.Id,
                    PostId = pid
                });
            }

            if ((byte) Convert.ToInt32(form["noflw"]) == 0)
            {
                var sitemap = new Models.SiteMap
                {
                    PostId = pid,
                    Type = (int) SiteMapType.Subpage,
                    Url = Url.RouteUrl("ShowPlainAndSpecialContent", new {slug = post.PostSlug}),
                    LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                    ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                };

                _ar.UpdateSiteMap(pid, sitemap.Type, sitemap);
            }
            else
            {
                _ar.DeleteSiteMapData(pid,(int)SiteMapType.Subpage);
            }

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 1});
        }

        #endregion

        #region Video

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddVideoPost(FormCollection form, HttpPostedFileBase image)
        {
            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);

                try
                {
                    var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                    var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                    var isExists = Directory.Exists(pathString);
                    if (!isExists)
                        Directory.CreateDirectory(pathString);

                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x 260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
                
                var post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    VideoId = form["videoid"],
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 2,
                    ReadCount = 0
                };

                var pid = _ar.AddPost(post);
                var tags = form["tags"].Split(',');
                foreach (var tag in tags)
                {
                    var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                    if (pid != null)
                        _ar.SavePostTagRefence(new PostTagReference
                        {
                            TagId = checkTag.Id,
                            PostId = (int) pid
                        });
                }

                if (pid != null && (byte) Convert.ToInt32(form["noflw"]) == 0)
                {
                    var sitemap = new Models.SiteMap
                    {
                        PostId = (int) pid,
                        Type = (int) SiteMapType.Subpage,
                        Url = Url.RouteUrl("ShowVideoContent", new {category = post.CatSlug, slug = post.PostSlug}),
                        LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                        ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                    };

                    _ar.AddSiteMap(sitemap);
                }
            }
            else
                return View("Post/PlainPost");


            TempData["Flash"] = true;

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 2});
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateVideoPost(FormCollection form, HttpPostedFileBase image)
        {
            var pid = Convert.ToInt32(form["pid"]);
            Post post;
            var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));

            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);
                try
                {
                    var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                    var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                    var isExists = Directory.Exists(pathString);
                    if (!isExists)
                        Directory.CreateDirectory(pathString);
                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    VideoId = form["videoid"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 2
                };
            }
            else
            {
                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = form["ex_image"],
                    ImageName = form["imageName"],
                    MobileImage = form["ex_image_mobile"],
                    ThumbImage = form["ex_image_thumb"],
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    VideoId = form["videoid"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 2
                };
            }

            _ar.UpdatePost(pid, post);
            var tags = form["tags"].Split(',');

            //Var olan referansları sil
            _ar.DeletePostTagReferences(pid);
            foreach (var tag in tags)
            {
                var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                _ar.SavePostTagRefence(new PostTagReference
                {
                    TagId = checkTag.Id,
                    PostId = pid
                });
            }

            if ((byte) Convert.ToInt32(form["noflw"]) == 0)
            {
                var sitemap = new Models.SiteMap
                {
                    PostId = pid,
                    Type = (int) SiteMapType.Subpage,
                    Url = Url.RouteUrl("ShowVideoContent", new {category = post.CatSlug, slug = post.PostSlug}),
                    LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                    ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                };

                _ar.UpdateSiteMap(pid, sitemap.Type, sitemap);
            }
            else
            {
                _ar.DeleteSiteMapData(pid,(int)SiteMapType.Subpage);
            }

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 2});
        }

        #endregion

        #region ListedPost

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddListedPost(FormCollection form, HttpPostedFileBase image,
            IEnumerable<HttpPostedFileBase> listImages)
        {
            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);


                var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                var isExists = Directory.Exists(pathString);
                if (!isExists)
                    Directory.CreateDirectory(pathString);

                try
                {
                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x 260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
                
                var post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 4,
                    ReadCount = 0
                };

                var pid = _ar.AddPost(post);
                var tags = form["tags"].Split(',');
                foreach (var tag in tags)
                {
                    var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                    if (pid != null)
                        _ar.SavePostTagRefence(new PostTagReference
                        {
                            TagId = checkTag.Id,
                            PostId = (int) pid
                        });
                }

                //List elements

                var i = 0;
                foreach (var elem in listImages)
                {
                    var n = form["listElements[" + i + "][elementAltText]"];
                    var listImageName = Utilities.ToSlug(n) + Path.GetExtension(elem.FileName);

                    var pathListImageName = $"{pathString}\\{listImageName}";
                    elem.SaveAs(pathListImageName);

                    //List image için resize
                    var web = new KalikoImage(pathListImageName);
                    var scale = web.Scale(new CropScaling(630, 450));
                    scale.SaveJpg(pathListImageName, 90);

                    Utilities.UploadToCdn(listImageName, pathListImageName);
                    System.IO.File.Delete(pathListImageName);

                    if (pid != null)
                    {
                        var le = new ListElement
                        {
                            ListTitle = form["listElements[" + i + "][elementTitle]"],
                            ImageAltText = form["listElements[" + i + "][elementAltText]"],
                            ListDescription = form["listElements[" + i + "][elementContent]"],
                            ListImage = listImageName,
                            PostId = (int) pid,
                            Ord = i + 1,
                            Status = 1
                        };

                        _ar.AddListedPost(le);
                    }

                    i++;
                }
                if (pid != null && (byte) Convert.ToInt32(form["noflw"]) == 0)
                {
                    var sitemap = new Models.SiteMap
                    {
                        PostId = (int) pid,
                        Type = (int) SiteMapType.Subpage,
                        Url = Url.RouteUrl("ShowListContent", new {category = post.CatSlug, slug = post.PostSlug}),
                        LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                        ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                    };

                    _ar.AddSiteMap(sitemap);
                }
            }
            else
                return View("Post/ListedPost");


            TempData["Flash"] = true;

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 4});
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateListedPost(FormCollection form, HttpPostedFileBase image,
            IEnumerable<HttpPostedFileBase> listImages)
        {
            var pid = Convert.ToInt32(form["pid"]);
            var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
            Post post;

            var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
            var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
            var isExists = Directory.Exists(pathString);
            if (!isExists)
                Directory.CreateDirectory(pathString);

            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);
                try
                {
                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 4
                };
            }
            else
            {
                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = form["ex_image"],
                    ImageName = form["imageName"],
                    MobileImage = form["ex_image_mobile"],
                    ThumbImage = form["ex_image_thumb"],
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 4
                };
            }

            _ar.UpdatePost(pid, post);

            var tags = form["tags"].Split(',');

            //Var olan referansları sil
            _ar.DeletePostTagReferences(pid);
            foreach (var tag in tags)
            {
                var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                _ar.SavePostTagRefence(new PostTagReference
                {
                    TagId = checkTag.Id,
                    PostId = pid
                });
            }

            _ar.DeletePostListElements(pid);

            var i = 0;
            foreach (var elem in listImages)
            {
                if (elem != null && elem.ContentLength > 0)
                {
                    var n = form["listElements[" + i + "][elementAltText]"];
                    var listImageName = Utilities.ToSlug(n) + Path.GetExtension(elem.FileName);

                    var pathListImageName = $"{pathString}\\{listImageName}";
                    elem.SaveAs(pathListImageName);

                    //List image için resize
                    var web = new KalikoImage(pathListImageName);
                    var scale = web.Scale(new CropScaling(630, 450));
                    scale.SaveJpg(pathListImageName, 90);

                    Utilities.UploadToCdn(listImageName, pathListImageName);
                    System.IO.File.Delete(pathListImageName);


                    var le = new ListElement
                    {
                        ListTitle = form["listElements[" + i + "][elementTitle]"],
                        ImageAltText = form["listElements[" + i + "][elementAltText]"],
                        ListDescription = form["listElements[" + i + "][elementContent]"],
                        ListImage = listImageName,
                        PostId = pid,
                        Ord = i + 1,
                        Status = 1
                    };

                    _ar.AddListedPost(le);
                }
                else
                {
                    var le = new ListElement
                    {
                        ListTitle = form["listElements[" + i + "][elementTitle]"],
                        ImageAltText = form["listElements[" + i + "][elementAltText]"],
                        ListDescription = form["listElements[" + i + "][elementContent]"],
                        ListImage = form["listElements[" + i + "][listImagesEx]"],
                        PostId = pid,
                        Ord = i + 1,
                        Status = 1
                    };

                    _ar.AddListedPost(le);
                }

                i++;
            }

            if ((byte) Convert.ToInt32(form["noflw"]) == 0)
            {
                var sitemap = new Models.SiteMap
                {
                    PostId = pid,
                    Type = (int) SiteMapType.Subpage,
                    Url = Url.RouteUrl("ShowListContent", new {category = post.CatSlug, slug = post.PostSlug}),
                    LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                    ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                };

                _ar.UpdateSiteMap(pid, sitemap.Type, sitemap);
            }else
            {
                _ar.DeleteSiteMapData(pid, (int) SiteMapType.Subpage);
            }
            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 4});
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddShopListedPost(FormCollection form, HttpPostedFileBase image,
            IEnumerable<HttpPostedFileBase> listImages)
        {
            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);


                var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                var isExists = Directory.Exists(pathString);
                if (!isExists)
                    Directory.CreateDirectory(pathString);

                try
                {
                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x 260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
                
                var post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 5,
                    ReadCount = 0
                };

                var pid = _ar.AddPost(post);
                var tags = form["tags"].Split(',');
                foreach (var tag in tags)
                {
                    var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                    if (pid != null)
                        _ar.SavePostTagRefence(new PostTagReference
                        {
                            TagId = checkTag.Id,
                            PostId = (int) pid
                        });
                }

                //List elements

                var i = 0;
                foreach (var elem in listImages)
                {
                    var n = form["listElements[" + i + "][elementAltText]"];
                    var listImageName = Utilities.ToSlug(n) + Path.GetExtension(elem.FileName);

                    var pathListImageName = $"{pathString}\\{listImageName}";
                    elem.SaveAs(pathListImageName);

                    //List image için resize
                    var web = new KalikoImage(pathListImageName);
                    var scale = web.Scale(new CropScaling(630, 450));
                    scale.SaveJpg(pathListImageName, 90);

                    Utilities.UploadToCdn(listImageName, pathListImageName);
                    System.IO.File.Delete(pathListImageName);

                    if (pid != null)
                    {
                        var le = new ListElement
                        {
                            ListTitle = form["listElements[" + i + "][elementTitle]"],
                            ImageAltText = form["listElements[" + i + "][elementAltText]"],
                            ListDescription = form["listElements[" + i + "][elementContent]"],
                            ListImage = listImageName,
                            Price = form["listElements[" + i + "][elementPrice]"],
                            WhereToSell = form["listElements[" + i + "][elementWhereToSell]"],
                            TargetLink = form["listElements[" + i + "][elementTargetLink]"],
                            NoFollow = (byte) Convert.ToInt32(form["listElements[" + i + "][elementNoFollow]"]),
                            PostId = (int) pid,
                            Ord = i + 1,
                            Status = 1
                        };

                        _ar.AddListedPost(le);
                    }

                    i++;
                }

                if (pid != null && (byte) Convert.ToInt32(form["noflw"]) == 0)
                {
                    var sitemap = new Models.SiteMap
                    {
                        PostId = (int) pid,
                        Type = (int) SiteMapType.Subpage,
                        Url = Url.RouteUrl("ShowShoppingContent", new {category = post.CatSlug, slug = post.PostSlug}),
                        LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                        ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                    };

                    _ar.AddSiteMap(sitemap);
                }
            }
            else
                return View("Post/ListedPost");


            TempData["Flash"] = true;

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 5});
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateShopListedPost(FormCollection form, HttpPostedFileBase image,
            IEnumerable<HttpPostedFileBase> listImages)
        {
            var pid = Convert.ToInt32(form["pid"]);
            var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
            Post post;

            var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
            var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
            var isExists = Directory.Exists(pathString);
            if (!isExists)
                Directory.CreateDirectory(pathString);

            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);
                try
                {
                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x 202
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 5
                };
            }
            else
            {
                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = form["ex_image"],
                    ImageName = form["imageName"],
                    MobileImage = form["ex_image_mobile"],
                    ThumbImage = form["ex_image_thumb"],
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 5
                };
            }

            _ar.UpdatePost(pid, post);

            var tags = form["tags"].Split(',');

            //Var olan referansları sil
            _ar.DeletePostTagReferences(pid);
            foreach (var tag in tags)
            {
                var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                _ar.SavePostTagRefence(new PostTagReference
                {
                    TagId = checkTag.Id,
                    PostId = pid
                });
            }

            _ar.DeletePostListElements(pid);

            var i = 0;
            foreach (var elem in listImages)
            {
                if (elem != null && elem.ContentLength > 0)
                {
                    var n = form["listElements[" + i + "][elementAltText]"];
                    var listImageName = Utilities.ToSlug(n) + Path.GetExtension(elem.FileName);

                    var pathListImageName = $"{pathString}\\{listImageName}";
                    elem.SaveAs(pathListImageName);

                    //List image için resize
                    var web = new KalikoImage(pathListImageName);
                    var scale = web.Scale(new CropScaling(630, 450));
                    scale.SaveJpg(pathListImageName, 90);

                    Utilities.UploadToCdn(listImageName, pathListImageName);
                    System.IO.File.Delete(pathListImageName);


                    var le = new ListElement
                    {
                        ListTitle = form["listElements[" + i + "][elementTitle]"],
                        ImageAltText = form["listElements[" + i + "][elementAltText]"],
                        ListDescription = form["listElements[" + i + "][elementContent]"],
                        ListImage = listImageName,
                        Price = form["listElements[" + i + "][elementPrice]"],
                        WhereToSell = form["listElements[" + i + "][elementWhereToSell]"],
                        TargetLink = form["listElements[" + i + "][elementTargetLink]"],
                        NoFollow = (byte) Convert.ToInt32(form["listElements[" + i + "][elementNoFollow]"]),
                        PostId = pid,
                        Ord = i + 1,
                        Status = 1
                    };

                    _ar.AddListedPost(le);
                }
                else
                {
                    var le = new ListElement
                    {
                        ListTitle = form["listElements[" + i + "][elementTitle]"],
                        ImageAltText = form["listElements[" + i + "][elementAltText]"],
                        ListDescription = form["listElements[" + i + "][elementContent]"],
                        ListImage = form["listElements[" + i + "][listImagesEx]"],
                        Price = form["listElements[" + i + "][elementPrice]"],
                        WhereToSell = form["listElements[" + i + "][elementWhereToSell]"],
                        TargetLink = form["listElements[" + i + "][elementTargetLink]"],
                        NoFollow = (byte) Convert.ToInt32(form["listElements[" + i + "][elementNoFollow]"]),
                        PostId = pid,
                        Ord = i + 1,
                        Status = 1
                    };

                    _ar.AddListedPost(le);
                }

                i++;
            }

            if ((byte) Convert.ToInt32(form["noflw"]) == 0)
            {
                var sitemap = new Models.SiteMap
                {
                    PostId = pid,
                    Type = (int) SiteMapType.Subpage,
                    Url = Url.RouteUrl("ShowShoppingContent", new {category = post.CatSlug, slug = post.PostSlug}),
                    LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                    ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                };

                _ar.UpdateSiteMap(pid, sitemap.Type, sitemap);
            }
            else
            {
                _ar.DeleteSiteMapData(pid, (int) SiteMapType.Subpage);
            }

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 5});
        }

        #endregion

        #region ImagePost

        public ActionResult AddImagePost(FormCollection form, HttpPostedFileBase image,
            IEnumerable<HttpPostedFileBase> listImages)
        {
            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);


                var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                var isExists = Directory.Exists(pathString);
                if (!isExists)
                    Directory.CreateDirectory(pathString);

                try
                {
                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x 260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 202));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);

                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
                
                var post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 3,
                    ReadCount = 0
                };

                var pid = _ar.AddPost(post);
                var tags = form["tags"].Split(',');
                foreach (var tag in tags)
                {
                    var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                    if (pid != null)
                        _ar.SavePostTagRefence(new PostTagReference
                        {
                            TagId = checkTag.Id,
                            PostId = (int) pid
                        });
                }

                //List elements

                var i = 0;
                foreach (var elem in listImages)
                {
                    var n = form["listElements[" + i + "][elementAltText]"];
                    var mainSliderImageName = Utilities.ToSlug(n) + Path.GetExtension(elem.FileName);
                    var sliderImageName = Utilities.ToSlug(n) + "-slider" + Path.GetExtension(elem.FileName);
                    var mobileSliderImageName = Utilities.ToSlug(n) + "-mobile" + Path.GetExtension(elem.FileName);


                    var pathMainSliderImageName = $"{pathString}\\{mainSliderImageName}";
                    elem.SaveAs(pathMainSliderImageName);

                    //Slider main image için resize
                    var main = new KalikoImage(pathMainSliderImageName);
                    var scaleMain = main.Scale(new CropScaling(980, 605));
                    scaleMain.SaveJpg(pathMainSliderImageName, 90);


                    var pathSliderImageName = $"{pathString}\\{sliderImageName}";
                    //Slider  için resize
                    var slider = new KalikoImage(pathMainSliderImageName);
                    var scaleSlider = slider.Scale(new CropScaling(630, 458));
                    scaleSlider.SaveJpg(pathSliderImageName, 90);


                    var pathMobileSliderImageName = $"{pathString}\\{mobileSliderImageName}";

                    //Mobile slider için resize
                    var mobile = new KalikoImage(pathMainSliderImageName);
                    var scaleMobile = mobile.Scale(new CropScaling(375, 372));
                    scaleMobile.SaveJpg(pathMobileSliderImageName, 90);


                    Utilities.UploadToCdn(mainSliderImageName, pathMainSliderImageName);
                    Utilities.UploadToCdn(sliderImageName, pathSliderImageName);
                    Utilities.UploadToCdn(mobileSliderImageName, pathMobileSliderImageName);

                    System.IO.File.Delete(pathMainSliderImageName);
                    System.IO.File.Delete(pathSliderImageName);
                    System.IO.File.Delete(pathMobileSliderImageName);

                    if (pid != null)
                    {
                        var pi = new PostImage
                        {
                            SlideText = form["listElements[" + i + "][elementSlideText]"],
                            ImageAltText = form["listElements[" + i + "][elementAltText]"],
                            Image = mainSliderImageName,
                            SliderImage = sliderImageName,
                            MobileImage = mobileSliderImageName,
                            OgImage = (byte) Convert.ToInt32(form["listElements[" + i + "][elementOgImage]"]),
                            PostId = (int) pid
                        };

                        _ar.AddListedImages(pi);
                    }

                    i++;
                }

                if (pid != null && (byte) Convert.ToInt32(form["noflw"]) == 0 )
                {
                    var sitemap = new Models.SiteMap
                    {
                        PostId = (int) pid,
                        Type = (int) SiteMapType.Subpage,
                        Url = Url.RouteUrl("ShowPhotoContent", new {category = post.CatSlug, slug = post.PostSlug}),
                        LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                        ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                    };

                    _ar.AddSiteMap(sitemap);
                }
            }
            else
                return View("Post/ImagePost");


            TempData["Flash"] = true;

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 3});
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateImagePost(FormCollection form, HttpPostedFileBase image,
            IEnumerable<HttpPostedFileBase> listImages)
        {
            var pid = Convert.ToInt32(form["pid"]);
            var d = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff")));
            Post post;

            var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
            var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
            var isExists = Directory.Exists(pathString);
            if (!isExists)
                Directory.CreateDirectory(pathString);

            if (image != null && image.ContentLength > 0)
            {
                var mainImageName = Utilities.ToSlug(Request.Form["imageName"]) + Path.GetExtension(image.FileName);
                var mobileImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-mobile" +
                                      Path.GetExtension(image.FileName);
                var thumbImageName = Utilities.ToSlug(Request.Form["imageName"]) + "-thumb" +
                                     Path.GetExtension(image.FileName);

                try
                {
                    var path = $"{pathString}\\{mainImageName}";
                    image.SaveAs(path);

                    //1440 x 650
                    //Desktop için resize
                    var web = new KalikoImage(path);
                    var scale = web.Scale(new CropScaling(1440, 650));
                    scale.SaveJpg(path, 90);


                    var pathMobile = $"{pathString}\\{mobileImageName}";

                    //375 x 260
                    //Mobile için resize
                    var mobile = new KalikoImage(path);
                    var thumbMobile = mobile.Scale(new CropScaling(375, 260));
                    thumbMobile.SaveJpg(pathMobile, 90);

                    var pathThumb = $"{pathString}\\{thumbImageName}";

                    //157 x 157
                    //Web thumb için resize
                    var thumb = new KalikoImage(path);
                    var thumbWeb = thumb.Scale(new CropScaling(157, 157));
                    thumbWeb.SaveJpg(pathThumb, 90);

                    Utilities.UploadToCdn(mainImageName, path);
                    Utilities.UploadToCdn(mobileImageName, pathMobile);
                    Utilities.UploadToCdn(thumbImageName, pathThumb);


                    System.IO.File.Delete(path);
                    System.IO.File.Delete(pathMobile);
                    System.IO.File.Delete(pathThumb);
                }
                catch (Exception ex)
                {
                    Logger(ex);
                }

                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = mainImageName,
                    ImageName = form["imageName"],
                    MobileImage = mobileImageName,
                    ThumbImage = thumbImageName,
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 3
                };
            }
            else
            {
                post = new Post
                {
                    PostTitle = form["title"],
                    MainImage = form["ex_image"],
                    ImageName = form["imageName"],
                    MobileImage = form["ex_image_mobile"],
                    ThumbImage = form["ex_image_thumb"],
                    ImageAltText = form["imageAltText"],
                    OgTitle = form["OgTitle"],
                    OgDescription = form["OgDescription"],
                    HtmlTitle = form["htmlTitle"],
                    HtmlMetaDesc = form["htmlMetaDesc"],
                    Canonical = form["canonical"],
                    PostSlug = form["slug"],
                    CatSlug = form["category"],
                    Credit = form["credit"],
                    PostShortDescription = form["shortdesc"],
                    PostContent = form["content"],
                    NoFollow = (byte) Convert.ToInt32(form["noflw"]),
                    CreatedDate = d,
                    PostStatus = Convert.ToByte(form["status"]),
                    PublishDate = string.IsNullOrEmpty(form["publishDate"])
                        ? d
                        : DateTime.Parse(form["publishDate"], new CultureInfo("tr-TR"), DateTimeStyles.AssumeLocal),
                    PostType = 3
                };
            }

            _ar.UpdatePost(pid, post);
            var tags = form["tags"].Split(',');

            //Var olan referansları sil
            _ar.DeletePostTagReferences(pid);
            foreach (var tag in tags)
            {
                var checkTag = _ar.CheckTagExist(Utilities.ToSlug(tag));

                _ar.SavePostTagRefence(new PostTagReference
                {
                    TagId = checkTag.Id,
                    PostId = pid
                });
            }

            _ar.DeletePostImageElements(pid);
            //List elements

            var i = 0;
            foreach (var elem in listImages)
            {
                if (elem != null && elem.ContentLength > 0)
                {
                    var n = form["listElements[" + i + "][elementAltText]"];
                    var mainSliderImageName = Utilities.ToSlug(n) + Path.GetExtension(elem.FileName);
                    var sliderImageName = Utilities.ToSlug(n) + "-slider" + Path.GetExtension(elem.FileName);
                    var mobileSliderImageName = Utilities.ToSlug(n) + "-mobile" + Path.GetExtension(elem.FileName);


                    var pathMainSliderImageName = $"{pathString}\\{mainSliderImageName}";
                    elem.SaveAs(pathMainSliderImageName);

                    //Slider main image için resize
                    var main = new KalikoImage(pathMainSliderImageName);
                    var scaleMain = main.Scale(new CropScaling(980, 605));
                    scaleMain.SaveJpg(pathMainSliderImageName, 90);


                    var pathSliderImageName = $"{pathString}\\{sliderImageName}";
                    //Slider  için resize
                    var slider = new KalikoImage(pathMainSliderImageName);
                    var scaleSlider = slider.Scale(new CropScaling(630, 458));
                    scaleSlider.SaveJpg(pathSliderImageName, 90);


                    var pathMobileSliderImageName = $"{pathString}\\{mobileSliderImageName}";

                    //Mobile slider için resize
                    var mobile = new KalikoImage(pathMainSliderImageName);
                    var scaleMobile = mobile.Scale(new CropScaling(375, 372));
                    scaleMobile.SaveJpg(pathMobileSliderImageName, 90);


                    Utilities.UploadToCdn(mainSliderImageName, pathMainSliderImageName);
                    Utilities.UploadToCdn(sliderImageName, pathSliderImageName);
                    Utilities.UploadToCdn(mobileSliderImageName, pathMobileSliderImageName);

                    System.IO.File.Delete(pathMainSliderImageName);
                    System.IO.File.Delete(pathSliderImageName);
                    System.IO.File.Delete(pathMobileSliderImageName);


                    var pi = new PostImage
                    {
                        SlideText = form["listElements[" + i + "][elementSlideText]"],
                        ImageAltText = form["listElements[" + i + "][elementAltText]"],
                        Image = mainSliderImageName,
                        SliderImage = sliderImageName,
                        MobileImage = mobileSliderImageName,
                        OgImage = (byte) Convert.ToInt32(form["listElements[" + i + "][elementOgImage]"]),
                        PostId = pid
                    };

                    _ar.AddListedImages(pi);
                }
                else
                {
                    var pi = new PostImage
                    {
                        SlideText = form["listElements[" + i + "][elementSlideText]"],
                        ImageAltText = form["listElements[" + i + "][elementAltText]"],
                        Image = form["listElements[" + i + "][listImagesEx]"],
                        SliderImage = form["listElements[" + i + "][listImagesExSlider]"],
                        MobileImage = form["listElements[" + i + "][listImagesExMobile]"],
                        OgImage = (byte) Convert.ToInt32(form["listElements[" + i + "][elementOgImage]"]),
                        PostId = pid
                    };

                    _ar.AddListedImages(pi);
                }

                i++;
            }

            if ((byte) Convert.ToInt32(form["noflw"]) == 0)
            {
                var sitemap = new Models.SiteMap
                {
                    PostId = pid,
                    Type = (int) SiteMapType.Subpage,
                    Url = Url.RouteUrl("ShowPhotoContent", new {category = post.CatSlug, slug = post.PostSlug}),
                    LastModified = DateTime.Now.AddHours(Convert.ToInt32(Utilities.AppSetting("TimeDiff"))),
                    ChangeFreq = Utilities.AppSetting("SiteMapCheckFreq")
                };


                _ar.UpdateSiteMap(pid, (int) SiteMapType.Subpage, sitemap);
            }
            else
            {
                _ar.DeleteSiteMapData(pid, (int) SiteMapType.Subpage);
            }

            TempData["Flash"] = true;

            return RedirectToAction("GetPostListByType", "Backoffice", new {type = 3});
        }

        #endregion
        
        #region SpecialContent
        public ActionResult ListSpecialContent()
        {
            var list = _ar.GetSpecialContents();
            return View("SpecialContent/SpeacialContentList", list);
        }
        
        public ActionResult ListSpecialContentPosts(int contentId)
        {
            var list = _ar.GetSpecialContentPosts(contentId);
            return View("SpecialContent/SpeacialContentListPosts", list);
        }

        public ActionResult AddSpecialContent()
        {
            return View();
        }

        public ActionResult UpdateSpecialContent()
        {
            return View();
        }

        public ActionResult DeleteSpecialContent()
        {
            return View();
        }
        #endregion

        #region Admins

        public ActionResult BackEndUser(int id = 0)
        {
            var admin = _ar.GetAdmin(id);

            return View("PanelUser/AddAdmin", admin);
        }

        [HttpPost]
        public ActionResult AddBackEndUser()
        {
            var admin = new BackEndUser
            {
                UserName = Request.Form["name"],
                Password = Utilities.Sha1(PasswordAdditional + "" + Request.Form["password"])
            };
            _ar.AddAdmin(admin);

            TempData["Flash"] = true;

            return RedirectToAction("ListBackEndUsers", "Backoffice");
        }

        [HttpPost]
        public ActionResult UpdateBackEndUser()
        {
            var admin = new BackEndUser
            {
                UserName = Request.Form["name"],
                Password = Request.Form["password"].Length < 40
                    ? Utilities.Sha1(PasswordAdditional + "" + Request.Form["password"])
                    : Request.Form["password"]
            };

            _ar.UpdateAdmin(Convert.ToInt32(Request.Form["aid"]), admin);

            TempData["Flash"] = true;

            return RedirectToAction("ListBackEndUsers", "Backoffice");
        }

        [HttpPost]
        public JsonResult DeleteBackEndUser(int id)
        {
            _ar.DeleteAdmin(id);
            TempData["Flash"] = true;
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListBackEndUsers()
        {
            var admins = _ar.ListAdmins();
            return View("PanelUser/AdminList", admins);
        }

        #endregion

        #region Tags

        public ActionResult Tag()
        {
            return View("Tag/AddTag");
        }

        [HttpPost]
        public ActionResult AddTag()
        {
            var checkTag = _ar.CheckTagExist(Utilities.ToSlug(Request.Form["tag"]));
            if (checkTag == null)
            {
                var tag = new Tag
                {
                    TagName = Request.Form["tag"],
                    TagSlug = Utilities.ToSlug(Request.Form["tag"])
                };
                _ar.AddTag(tag);

                TempData["Flash"] = true;
            }
            else
            {
                TempData["Flash"] = false;
            }

            return RedirectToAction("ListTags", "Backoffice");
        }

        [HttpPost]
        public JsonResult DeleteTag(int id)
        {
            _ar.DeleteTag(id);
            _ar.DeleteTagReferences(id);
            TempData["Flash"] = true;
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListTags()
        {
            var tags = _ar.ListTags();
            return View("Tag/TagList", tags);
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Log yardımcısı
        /// </summary>
        /// <param name="ex"></param>
        private static void Logger(Exception ex)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Error(ex);
        }

        /// <summary>
        /// Dropzone upload helper
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveUploadedFile()
        {
            var isSavedSuccessfully = true;
            var fName = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    var file = Request.Files[fileName];
                    //Save file content goes here
                    if (file == null) continue;
                    fName = file.FileName;
                    if (file.ContentLength <= 0) continue;
                    var originalDirectory = new DirectoryInfo($"{Server.MapPath(@"\")}Upload");
                    var pathString = Path.Combine(originalDirectory.ToString(), "Temp");
                    var isExists = Directory.Exists(pathString);
                    if (!isExists)
                        Directory.CreateDirectory(pathString);
                    var fileExtention = Path.GetExtension(fName);
                    var fn = Guid.NewGuid().ToString();
                    var newName = fn + fileExtention;
                    var path = $"{pathString}\\{newName}";
                    file.SaveAs(path);
                    Utilities.UploadToCdn(newName, path);
                    System.IO.File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                var logger = LogManager.GetCurrentClassLogger();
                logger.Error(ex);
                isSavedSuccessfully = false;
            }
            return Json(isSavedSuccessfully ? new {Message = fName} : new {Message = "Error in saving file"});
        }

        /// <summary>
        /// Çıkış
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session["Admin"] = null;
            return Redirect("/");
        }

        private static int SetBegin(int page, int ps)
        {
            return page == 1 ? page : (page - 1) * ps + 1;
        }

        private static int SetEnd(int begin, int page, int ps)
        {
            return begin == 1 ? 10 : page * ps;
        }

        #endregion

        #region Settings

        public ActionResult TrackingCodes()
        {
            var settings = _ar.SiteSettings(1);
            return View("Setting/TrackingCodes", settings);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateTrackingCodes(FormCollection form)
        {
            var setting = new SiteSetting
            {
                GaCode = form["gacode"],
                FpCode = form["fbcode"]
            };

            _ar.UpdateTrackingCodes(Convert.ToInt32(Request.Form["sid"]), setting);

            TempData["Flash"] = true;

            return RedirectToAction("TrackingCodes", "Backoffice");
        }

        public ActionResult MainMetaData()
        {
            var settings = _ar.SiteSettings(1);
            return View("Setting/MetaTags", settings);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateMainMetaData(FormCollection form)
        {
            var setting = new SiteSetting
            {
                MetaTitle = form["metatitle"],
                MetaDescription = form["metadesc"]
            };

            _ar.UpdateMetaCodes(Convert.ToInt32(Request.Form["sid"]), setting);

            TempData["Flash"] = true;

            return RedirectToAction("MainMetaData", "Backoffice");
        }

        public ActionResult Redirection()
        {
            return View("Setting/Redirect");
        }

        [HttpPost]
        public ActionResult AddRedirection(FormCollection form)
        {
            var redirection = new Redirection
            {
                OldUrl = form["oldurl"],
                NewUrl = form["newurl"],
                RedirectType = form["type"],
                Active = Convert.ToBoolean(form["status"])
            };

            _ar.AddRedirect(redirection);

            TempData["Flash"] = true;

            return RedirectToAction("ListRedirects", "Backoffice");
        }

        public ActionResult ListRedirects()
        {
            var listRedirects = _ar.ListRedirects();
            return View("Setting/RedirectList", listRedirects);
        }

        [HttpPost]
        public JsonResult DeleteRedirection(int id)
        {
            _ar.DeleteRedirect(id);
            TempData["Flash"] = true;
            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
