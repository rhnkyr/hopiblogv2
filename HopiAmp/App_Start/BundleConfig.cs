﻿using System.Web.Optimization;

namespace HopiAmp
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
           
            bundles.Add(new ScriptBundle("~/bundles/libs").Include(
                "~/Static/fe/assets/js/jquery.js",
                "~/Static/fe/assets/js/jquery.easing.min.js",
                "~/Static/fe/assets/js/jquery.maskedinput.min.js",
                "~/Static/fe/assets/js/owl.carousel.min.js",
                "~/Static/fe/assets/js/jquery.fancybox.pack.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                "~/Static/fe/assets/js/hopi-blog.js"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Static/fe/assets/css/animate.css",
                "~/Static/fe/assets/css/hb-main.css",
                "~/Static/fe/assets/css/hb-amp.css"
                ));


            BundleTable.EnableOptimizations = true;
        }
    }
}
