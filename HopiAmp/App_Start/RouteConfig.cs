﻿using System.Web.Mvc;
using System.Web.Routing;

namespace HopiAmp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //routes.MapMvcAttributeRoutes();

            //routes.IgnoreRoute("Content/{*pathInfo}");

          
            routes.MapRoute(
                "Sitemap",
                "sitemap",
                new {controller = "Home", action = "SiteMap"}
            );
            
            routes.MapRoute(
                "Search",
                "arama",
                new {controller = "Home", action = "Search"}
            );

            routes.MapRoute(
                "ShowPhotoContent",
                "fotograf-galerisi/{category}/{slug}",
                new
                {
                    controller = "Home",
                    action = "ShowContent"
                }
            );

            routes.MapRoute(
                "ShowVideoContent",
                "video/{category}/{slug}",
                new
                {
                    controller = "Home",
                    action = "ShowContent"
                }
            );

         
            routes.MapRoute(
                "ShowListContent",
                "liste/{category}/{slug}",
                new {controller = "Home", action = "ShowContent"}
            );
           
            routes.MapRoute(
                "ShowTagContent",
                "etiket/{slug}",
                new {controller = "Home", action = "ShowTagContent", slug = UrlParameter.Optional}
            );

            routes.MapRoute(
                "ShowCategoryContent",
                "kategori/{slug}",
                new {controller = "Home", action = "ShowCategoryContent"}
            );
            
            routes.MapRoute(
                "ShowShoppingContent",
                "alisveris/{slug}",
                new {controller = "Home", action = "ShowContent"}
            );

            routes.MapRoute(
                "ShowPlainAndSpecialContent",
                "icerik/{slug}",
                new {controller = "Home", action = "ShowContent"}
            );

            /*routes.MapRoute(
                "Preview",
                "{slug}/on-izleme",
                new {controller = "Home", action = "Preview"}
            );*/
           
            
            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                new {controller = "Home|Backoffice|AdminHome|Error"}
            );

            //catchAll routes to grab the URLs we want to redirect

            routes.MapRoute(
                "Redirect_OrError",
                "{*CatchAll1*}",
                new {controller = "Error", action = "NotFound"}
            );

            routes.MapRoute(
                "Redirect_OrError_two_levels",
                "{CatchAll1*}/{CatchAll2*}",
                new {controller = "Error", action = "NotFound"}
            );

            routes.MapRoute(
                "Redirect_OrError_three_levels",
                "{CatchAll1*}/{CatchAll2*}/{CatchAll3*}",
                new {controller = "Error", action = "NotFound"}
            );
            routes.MapRoute(
                "Redirect_OrError_four_levels",
                "{CatchAll1*}/{CatchAll2*}/{CatchAll3*}/{CatchAll4*}",
                new {controller = "Error", action = "NotFound"}
            );
        }
    }
}
