﻿using System.Collections.Generic;
using HopiAmp.Models;

namespace HopiAmp.ViewModels.Site
{
    public class HomePageViewModel
    {
        public IEnumerable<Slider> Sliders { get; set; }
        public IEnumerable<Post> LatestPosts { get; set; }
        public IEnumerable<object> MostRead { get; set; }
    }
}
