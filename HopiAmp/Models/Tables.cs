﻿using System;
using System.Web.Mvc;
using Dapper;

namespace HopiAmp.Models
{
    [Table("Categories")]
    public class Category
    {
        public int Id { get; set; }

        public string CatName { get; set; }

        public string CatSlug { get; set; }
    }

    [Table("ListElements")]
    public class ListElement
    {
        public int Id { get; set; }

        public int PostId { get; set; }
        
        public string ListTitle { get; set; }

        public string ListImage { get; set; }
        
        public string ImageAltText { get; set; }
        
        public string ListDescription { get; set; }
        
        public string TargetLink { get; set; }
        
        public string Price { get; set; }
        
        public string WhereToSell { get; set; }
        
        public int Status { get; set; }
        
        public int Ord { get; set; }
        
        public byte NoFollow { get; set; }

    }

    [Table("Posts")]
    public class Post
    {
        public int Id { get; set; }

        public byte PostType { get; set; }

        public string PostTitle { get; set; }

        public string PostSlug { get; set; }

        public string PostShortDescription { get; set; }

        public byte PostStatus { get; set; }
        
        public byte NoFollow { get; set; }

        public string MainImage { get; set; }

        public string MobileImage { get; set; }

        public string ThumbImage { get; set; }

        public string ImageAltText { get; set; }

        public string HtmlTitle { get; set; }

        public string HtmlMetaDesc { get; set; }

        public string ImageName { get; set; }

        public string Credit { get; set; }
        
        public string Canonical { get; set; }

        [AllowHtml]
        public string PostContent { get; set; }

        public string CatSlug { get; set; }

        public string VideoId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime PublishDate { get; set; }

        public int ReadCount { get; set; }
    }

    [Table("PostImages")]
    public class PostImage
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public string Image { get; set; }

        public string MobileImage { get; set; }

        public string SliderImage { get; set; }
        
        public string ImageAltText { get; set; }
        
        [AllowHtml]
        public string SlideText { get; set; }
    }


    [Table("Sliders")]
    public class Slider
    {
        public int Id { get; set; }

        public string Image { get; set; }

        public string ImageMobile { get; set; }

        public string ImageName { get; set; }

        public string ImageAltText { get; set; }

        public string Link { get; set; }
        
        public byte NoFollow { get; set; }

        [AllowHtml]
        public string TopText { get; set; }

        [AllowHtml]
        public string BottomText { get; set; }

        public int Ord { get; set; }
    }

    [Table("Tags")]
    public class Tag
    {
        public int Id { get; set; }

        public string TagName { get; set; }

        public string TagSlug { get; set; }
    }

    [Table("PostTagReferences")]
    public class PostTagReference
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public int TagId { get; set; }
    }

    [Table("Subscriptions")]
    public class Subscription
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    [Table("SiteSettings")]
    public class SiteSetting
    {
        public int Id { get; set; }

        [AllowHtml]
        public string GaCode { get; set; }

        [AllowHtml]
        public string FpCode { get; set; }
        
        public string MetaTitle { get; set; }
        
        public string MetaDescription { get; set; }
    }

    [Table("Admins")]
    public class BackEndUser
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
    
    [Table("Redirections")]
    public class Redirection
    {
        public int Id { get; set; }
        public string OldUrl { get; set; }
        public string NewUrl { get; set; }
        public string RedirectType { get; set; }
        public bool? Active { get; set; }
    }
    
   
    public class SiteMap
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int Type { get; set; }
        public string Url { get; set; }
        public string Priority { get; set; }
        public DateTime LastModified { get; set; }
        public string ChangeFreq { get; set; }
    }
}
