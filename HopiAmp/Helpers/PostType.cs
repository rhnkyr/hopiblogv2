﻿namespace HopiAmp.Helpers
{
    public enum PostType
    {
        PlainText = 1,
        Video = 2,
        Image = 3,
        List = 4,
        ShopList = 5
    }
}
