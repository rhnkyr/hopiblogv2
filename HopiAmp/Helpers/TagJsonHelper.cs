﻿namespace HopiAmp.Helpers
{
    public class TagJsonHelper
    {
        public int Id { get; set; }
        public string TagName { get; set; }
    }
}
