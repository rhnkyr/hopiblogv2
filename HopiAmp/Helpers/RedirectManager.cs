﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using HopiAmp.Models;

namespace HopiAmp.Helpers
{
    public static class RedirectManager
    {
        public static Redirection GetRedirectFrom(string oldUrl)
        {
            using (IDbConnection db = new SqlConnection(Utilities.ConnectionString("HopiBlogConnection")))
            {
                var urlWithoutSlash = oldUrl;
                if (!urlWithoutSlash.StartsWith("/"))
                {
                    urlWithoutSlash = '/' + urlWithoutSlash;
                }

                var redirects = db.QueryFirstOrDefault<Redirection>(
                    "SELECT * FROM Redirections WHERE OldUrl=@urlWithoutSlash AND Active = 1", new {urlWithoutSlash});

                if (redirects != null)
                {
                    return new Redirection
                    {
                        OldUrl = redirects.OldUrl,
                        NewUrl = redirects.NewUrl,
                        Active = redirects.Active,
                        RedirectType = redirects.RedirectType
                    };
                }

                return null;
            }
        }
    }
}
