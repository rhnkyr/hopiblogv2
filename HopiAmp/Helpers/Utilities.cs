﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using NLog;
using NLog.Internal;

namespace HopiAmp.Helpers
{
    public class Utilities
    {
        static Regex MobileCheck = new Regex(
            @"android|(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino",
            RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);

        static Regex MobileVersionCheck = new Regex(
            @"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-",
            RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);

        public static bool BrowserIsMobile()
        {
            Debug.Assert(HttpContext.Current != null);

            if (HttpContext.Current.Request == null ||
                HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"] == null) return false;

            var u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];

            if (u.Length < 4)
                return false;

            return MobileCheck.IsMatch(u) || MobileVersionCheck.IsMatch(u.Substring(0, 4));
        }

        /// <summary>
        /// ConnectionString yardımcısı
        /// </summary>
        /// <returns></returns>
        public static string ConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }

        /// <summary>
        /// AppSetting yardımcısı
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// Password has helper
        /// </summary>
        /// <param name="clearText"></param>
        /// <returns></returns>
        public static string ConvertStringToMd5(string clearText)
        {
            var byteData = Encoding.ASCII.GetBytes(clearText);
            var oMd5 = MD5.Create();
            var hashData = oMd5.ComputeHash(byteData);
            var oSb = new StringBuilder();
            foreach (var t in hashData)
                oSb.Append(t.ToString("x2"));
            return oSb.ToString();
        }

        /// <summary>
        /// Şifre sha1 üretir
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Sha1(string input)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }

        /// <summary>
        /// Türkçe karakteleri ingilizceye çevirir
        /// </summary>
        /// <param name="tr"></param>
        /// <returns></returns>
        public static string ToEnglish(string tr)
        {
            tr = tr.Replace('ö', 'o');
            tr = tr.Replace('Ö', 'O');
            tr = tr.Replace('ü', 'u');
            tr = tr.Replace('Ü', 'U');
            tr = tr.Replace('ğ', 'g');
            tr = tr.Replace('ş', 's');
            tr = tr.Replace('Ş', 'S');
            tr = tr.Replace('ı', 'i');
            tr = tr.Replace('İ', 'i');
            tr = tr.Replace('Ç', 'c');
            tr = tr.Replace('ç', 'c');
            tr = tr.Replace(' ', '-');

            return tr;
        }


        //http://www.joemoceri.com/uploading-files-to-ftp-using-csharp/
        /// <summary>
        /// Görselleri cdn e yükler
        /// </summary>
        /// <param name="newFileName"></param>
        /// <param name="localFilePath"></param>
        public static bool UploadToCdn(string newFileName, string localFilePath)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(AppSetting("CDNUser"), AppSetting("CDNPass"));
                    client.UploadFile(AppSetting("CDNUrl") + "/" + newFileName, localFilePath);
                }
                
                return true;
            }
            catch (Exception e)
            {
                var logger = LogManager.GetCurrentClassLogger();
                logger.Error(e.ToString);
                return false;
            }
        }

        /// <summary>
        /// Textleri seo slug hale getirir
        /// </summary>
        /// <param name="prmStr"></param>
        /// <returns></returns>
        public static string ToSlug(string prmStr)
        {
            if (string.IsNullOrEmpty(prmStr))
                return "";

            var link = new StringBuilder();
            prmStr = prmStr.ToLower();
            foreach (var t in prmStr)
            {
                if (t >= 'a' && t <= 'z' || t >= '0' && t <= '9' || t >= 'A' && t <= 'Z')
                {
                    link.Append(t);
                }
                else
                {
                    switch (t)
                    {
                        case 'ç':
                            link.Append("c");
                            break;

                        case 'ı':
                            link.Append("i");
                            break;
                        case 'İ':
                            link.Append("i");
                            break;
                        case 'ğ':
                            link.Append("g");
                            break;

                        case 'ö':
                            link.Append("o");
                            break;

                        case 'ô':
                            link.Append("o");
                            break;
                        case 'ü':
                            link.Append("u");
                            break;

                        case 'ş':
                            link.Append("s");
                            break;
                        default:
                            link.Append("-");
                            break;
                    }
                }
            }
            var seozed = link.ToString();
            while (seozed.IndexOf("--", StringComparison.Ordinal) != -1)
            {
                seozed = seozed.Replace("--", "-");
            }

            return seozed;
        }
    }
}
