﻿using System.Collections.Generic;
using Dapper;
using HopiBlog.Helpers;
using HopiBlog.Models;

namespace HopiBlog.Repository
{
    public class AdminRepository : BaseRepository
    {
        public AdminRepository(string connectionString) : base(connectionString)
        {
        }

        /*http://stackoverflow.com/questions/5160307/how-can-i-create-a-tagging-system-using-php-and-mysql*/

        /// <summary>
        /// Admin sorgulama
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public BackEndUser CheckAdmin(string username, string password)
        {
            return GetConnection(db =>
            {
                var admin = db.QueryFirstOrDefault<BackEndUser>(
                    "SELECT * FROM Admins WHERE UserName = @e AND Password = @p", new {e = username, p = password});
                return admin;
            });
        }

        #region Tags

        /// <summary>
        /// Etiket kayıt
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public void AddTag(Tag tag)
        {
            GetConnection(db =>
            {
                var ret = db.Insert(tag);
                return ret;
            });
        }

        /// <summary>
        /// Etiketleri listeler
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tag> ListTags()
        {
            return GetConnection(db => db.GetList<Tag>("ORDER BY TagName ASC"));
        }


        /// <summary>
        /// Etiket sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void DeleteTag(int id)
        {
            var tag = new Tag {Id = id};
            GetConnection(db => db.Delete(tag));
        }

        /// <summary>
        /// Post tag ilişkilerini temizler
        /// </summary>
        /// <param name="tagid">Post id</param>
        /// <returns></returns>
        public bool DeleteTagReferences(int tagid)
        {
            return GetConnection(db =>
            {
                var ret = db.DeleteList<PostTagReference>("WHERE TagId=@tagid", new {tagid});
                return ret > 0;
            });
        }

        #endregion

        #region Post

        /// <summary>
        /// Tüm postları getirir
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Post> GetAllPosts()
        {
            return GetConnection(db =>
            {
                var posts = db.Query<Post>("SELECT Id, PostType FROM Posts");
                return posts;
            });
        }

        /// <summary>
        /// Blog girdisi için  input yardımcı json datası
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<TagJsonHelper> GetTags(string key)
        {
            return GetConnection(db =>
            {
                var l = "%" + key + "%";
                var tags = db.Query<TagJsonHelper>(
                    "SELECT Id, TagName FROM Tags WHERE TagName LIKE @l", new {l});
                return tags;
            });
        }

        /// <summary>
        /// Kategorileri getirir
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Category> GetCategories()
        {
            return GetConnection(db =>
            {
                var categories = db.GetList<Category>("ORDER BY CatName ASC");
                return categories;
            });
        }

        /// <summary>
        /// Türe göre postları listeler
        /// </summary>
        /// <param name="type"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetPostListByType(int type, int begin, int end)
        {
            return GetConnection(db =>
            {
                const string sql = @"WITH P AS
                                    (
                                      SELECT Id, PostTitle, PostStatus, PostSlug, PostType, CreatedDate, PublishDate, MobileImage,
                                        ROW_NUMBER() OVER (ORDER BY p.CreatedDate DESC) AS 'RowNumber'
                                        FROM Posts p
                                        WHERE PostType = @type
                                    )
                                    SELECT *
                                    FROM P
                                    WHERE RowNumber BETWEEN @begin AND @end";

                var list = db.Query<Post>(sql, new {type, begin, end});
                return list;
            });
        }

        /// <summary>
        /// Arama sonuçları getirir
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetPostListByKey(string word)
        {
            return GetConnection(db =>
            {
                const string sql = @"SELECT
                                        Id, PostTitle, PostStatus, PostSlug, PostType, CreatedDate, PublishDate, MobileImage
                                    FROM
                                        Posts 
                                    WHERE
                                        PostStatus = 1 AND  PostTitle LIKE @lQuery
                                    ORDER BY
                                        PostTitle";

                var posts = db.Query<Post>(sql, new {lQuery = "%" + word + "%"});

                return posts;
            });
        }


        /// <summary>
        /// Toplam içerik
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int TotalPosts(int type)
        {
            return GetConnection(db =>
            {
                var total = db.RecordCount<Post>("WHERE PostType=@type", new {type});
                return total;
            });
        }

        /// <summary>
        /// Post içeriğini getirir
        /// </summary>
        /// <param name="pid">Post id</param>
        /// <returns></returns>
        public Post GetPost(int pid)
        {
            return GetConnection(db =>
            {
                var post = db.QueryFirstOrDefault<Post>("SELECT * FROM Posts WHERE Id=@pid", new {pid});
                return post;
            });
        }

        /// <summary>
        /// Listeleri elementlerini getirir
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public IEnumerable<ListElement> GetListElements(int postId)
        {
            return GetConnection(db =>
            {
                var listElements = db.GetList<ListElement>("WHERE PostId=@postId", new {postId});
                return listElements;
            });
        }

        /// <summary>
        /// Galeri Postun görsellerini getirir
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public IEnumerable<PostImage> GetPostImages(int postId)
        {
            return GetConnection(db =>
            {
                var listElements = db.GetList<PostImage>("WHERE PostId=@postId", new {postId});
                return listElements;
            });
        }

        /// <summary>
        /// Plain post kayıt
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public int? AddPost(Post post)
        {
            return GetConnection(db =>
            {
                var ret = db.Insert(post);
                return ret;
            });
        }

        /// <summary>
        /// Plain post güncelleme
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="post"></param>
        /// <returns></returns>
        public void UpdatePost(int postId, Post post)
        {
            GetConnection(db =>
            {
                var p = GetPost(postId);

                p.PostTitle = post.PostTitle;
                p.MainImage = post.MainImage;
                p.ImageName = post.ImageName;
                p.MobileImage = post.MobileImage;
                p.ThumbImage = post.ThumbImage;
                p.ImageAltText = post.ImageAltText;
                p.OgTitle = post.OgTitle;
                p.OgDescription = post.OgDescription;
                p.HtmlTitle = post.HtmlTitle;
                p.HtmlTitle = post.HtmlTitle;
                p.HtmlMetaDesc = post.HtmlMetaDesc;
                p.Canonical = post.Canonical;
                p.NoFollow = post.NoFollow;
                p.PostSlug = post.PostSlug;
                p.CatSlug = post.CatSlug;
                p.Credit = post.Credit;
                p.VideoId = post.VideoId;
                p.PostShortDescription = post.PostShortDescription;
                p.PostContent = post.PostContent;
                p.CreatedDate = post.CreatedDate;
                p.PostStatus = post.PostStatus;
                p.PublishDate = post.PublishDate;
                p.PostType = post.PostType;

                var ret = db.Update(p);
                return ret;
            });
        }

        /// <summary>
        /// List için ek element ekleme işlemi
        /// </summary>
        /// <param name="listElement"></param>
        /// <returns></returns>
        public int? AddListedPost(ListElement listElement)
        {
            return GetConnection(db =>
            {
                var ret = db.Insert(listElement);
                return ret;
            });
        }

        /// <summary>
        /// Görsel slider post için görsel ekleme ekleme işlemi
        /// </summary>
        /// <param name="postImage"></param>
        /// <returns></returns>
        public int? AddListedImages(PostImage postImage)
        {
            return GetConnection(db =>
            {
                var ret = db.Insert(postImage);
                return ret;
            });
        }

        /// <summary>
        /// Post Siler
        /// </summary>
        /// <param name="id"></param>
        public void DeletePost(int id)
        {
            var post = new Post() {Id = id};
            GetConnection(db => db.Delete(post));
        }

        /// <summary>
        /// Tag kayıt
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public int? SaveTag(Tag tag)
        {
            return GetConnection(db =>
            {
                var ret = db.Insert(tag);
                return ret;
            });
        }

        /// <summary>
        /// Post tag ilişki datası kayıt
        /// </summary>
        /// <param name="postTagReference"></param>
        /// <returns></returns>
        public bool SavePostTagRefence(PostTagReference postTagReference)
        {
            return GetConnection(db =>
            {
                var ret = db.Insert(postTagReference);
                return ret > 0;
            });
        }

        /// <summary>
        /// Post tag ilişkilerini temizler
        /// </summary>
        /// <param name="postId">Post id</param>
        /// <returns></returns>
        public bool DeletePostTagReferences(int postId)
        {
            return GetConnection(db =>
            {
                var ret = db.DeleteList<PostTagReference>("WHERE PostId=@postId", new {postId});
                return ret > 0;
            });
        }

        /// <summary>
        /// List Post elementlerini temizler
        /// </summary>
        /// <param name="postId">Post id</param>
        /// <returns></returns>
        public void DeletePostListElements(int postId)
        {
            GetConnection(db =>
            {
                var ret = db.DeleteList<ListElement>("WHERE PostId=@postId", new {postId});
                return ret > 0;
            });
        }

        /// <summary>
        /// List Image elementlerini temizler
        /// </summary>
        /// <param name="postId">Post id</param>
        /// <returns></returns>
        public void DeletePostImageElements(int postId)
        {
            GetConnection(db =>
            {
                var ret = db.DeleteList<PostImage>("WHERE PostId=@postId", new {postId});
                return ret > 0;
            });
        }

        /// <summary>
        /// Girilen Tag var olan bir kayıt olup olmadığını kontrol eder
        /// </summary>
        /// <param name="slug">Tag slug</param>
        /// <returns></returns>
        public Tag CheckTagExist(string slug)
        {
            return GetConnection(db =>
            {
                var ret = db.QueryFirstOrDefault<Tag>("SELECT * FROM Tags WHERE TagSlug=@slug", new {slug});
                return ret;
            });
        }

        /// <summary>
        /// Post için ilgili tagleri getirir
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public IEnumerable<Tag> GetPostTags(int postId)
        {
            return GetConnection(db =>
            {
                var tags = db.Query<Tag>(
                    "SELECT TagName FROM Tags INNER JOIN PostTagReferences ON PostTagReferences.TagId = Tags.Id WHERE PostId = @postId",
                    new {postId});
                return tags;
            });
        }

        #endregion

        #region Sliders

        /// <summary>
        /// Slider kayıt
        /// </summary>
        /// <param name="slider"></param>
        /// <returns></returns>
        public void AddSlider(Slider slider)
        {
            GetConnection(db =>
            {
                var ret = db.Insert(slider);
                return ret;
            });
        }

        /// <summary>
        /// Slider getirir
        /// </summary>
        /// <param name="sid">Post id</param>
        /// <returns></returns>
        public Slider GetSlider(int sid)
        {
            return GetConnection(db =>
            {
                var slider = db.QueryFirstOrDefault<Slider>("SELECT * FROM Sliders WHERE Id=@sid", new {sid});
                return slider;
            });
        }

        /// <summary>
        /// Slider güncelleme
        /// </summary>
        /// <param name="sliderId"></param>
        /// <param name="slider"></param>
        /// <returns></returns>
        public void UpdateSlider(int sliderId, Slider slider)
        {
            GetConnection(db =>
            {
                var s = GetSlider(sliderId);
                s.Image = slider.Image;
                s.ImageMobile = slider.ImageMobile;
                s.ImageName = slider.ImageName;
                s.ImageAltText = slider.ImageAltText;
                s.TopText = slider.TopText;
                s.BottomText = slider.BottomText;
                s.Link = slider.Link;
                s.NoFollow = slider.NoFollow;
                var ret = db.Update(s);
                return ret;
            });
        }

        /// <summary>
        /// Slider güncelleme
        /// </summary>
        /// <param name="sliderId"></param>
        /// <param name="slider"></param>
        /// <returns></returns>
        public void UpdateSliderOrder(int sliderId, Slider slider)
        {
            GetConnection(db =>
            {
                var s = GetSlider(sliderId);
                s.Ord = slider.Ord;
                var ret = db.Update(s);
                return ret;
            });
        }

        /// <summary>
        /// Slider listeler
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Slider> ListSliders()
        {
            return GetConnection(db => db.GetList<Slider>());
        }


        /// <summary>
        /// Slider sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void DeleteSlider(int id)
        {
            var slider = new Slider {Id = id};
            GetConnection(db => db.Delete(slider));
        }

        #endregion

        #region Catergory

        /// <summary>
        /// Kategori kayıt
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public int? AddCategory(Category category)
        {
            return GetConnection(db =>
            {
                var ret = db.Insert(category);
                return ret;
            });
        }

        /// <summary>
        /// Kategori getirir
        /// </summary>
        /// <param name="cid">Category id</param>
        /// <returns></returns>
        public Category GetCategory(int cid)
        {
            return GetConnection(db =>
            {
                var category = db.QueryFirstOrDefault<Category>("SELECT * FROM Categories WHERE Id=@cid", new {cid});
                return category;
            });
        }

        /// <summary>
        /// Kategorş güncelleme
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public void UpdateCategory(int cid, Category category)
        {
            GetConnection(db =>
            {
                var s = GetCategory(cid);
                s.CatName = category.CatName;
                s.CatSlug = category.CatSlug;
                var ret = db.Update(s);
                return ret;
            });
        }

        /// <summary>
        /// Kategori sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void DeleteCategory(int id)
        {
            var category = new Category {Id = id};
            GetConnection(db => db.Delete(category));
        }

        /// <summary>
        /// Kategori listesi
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Category> ListCategories()
        {
            return GetConnection(db => db.GetList<Category>());
        }

        #endregion

        #region Admins

        /// <summary>
        /// Admin kayıt
        /// </summary>
        /// <param name="admin"></param>
        /// <returns></returns>
        public void AddAdmin(BackEndUser admin)
        {
            GetConnection(db =>
            {
                var ret = db.Insert(admin);
                return ret;
            });
        }

        /// <summary>
        /// Admin getirir
        /// </summary>
        /// <param name="aid">Post id</param>
        /// <returns></returns>
        public BackEndUser GetAdmin(int aid)
        {
            return GetConnection(db =>
            {
                var admin = db.QueryFirstOrDefault<BackEndUser>("SELECT * FROM Admins WHERE Id=@aid", new {aid});
                return admin;
            });
        }

        /// <summary>
        /// Admin güncelleme
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="admin"></param>
        /// <returns></returns>
        public void UpdateAdmin(int adminId, BackEndUser admin)
        {
            GetConnection(db =>
            {
                var s = GetAdmin(adminId);
                s.UserName = admin.UserName;
                s.Password = admin.Password;
                var ret = db.Update(s);
                return ret;
            });
        }

        /// <summary>
        /// Admin sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void DeleteAdmin(int id)
        {
            var admin = new BackEndUser {Id = id};
            GetConnection(db => db.Delete(admin));
        }

        /// <summary>
        /// Yönetici listesi
        /// </summary>
        /// <returns></returns>
        public IEnumerable<BackEndUser> ListAdmins()
        {
            return GetConnection(db => db.GetList<BackEndUser>());
        }

        #endregion

        #region SiteSettings

        /// <summary>
        /// Site ayarlarını getiri
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public SiteSetting SiteSettings(int i)
        {
            return GetConnection(db =>
            {
                var setting = db.QueryFirstOrDefault<SiteSetting>("SELECT * FROM SiteSettings WHERE Id=@i", new {i});
                return setting;
            });
        }

        /// <summary>
        /// Tracking code güncelleme
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="setting"></param>
        public void UpdateTrackingCodes(int sid, SiteSetting setting)
        {
            GetConnection(db =>
            {
                var s = SiteSettings(sid);
                s.GaCode = setting.GaCode;
                s.FpCode = setting.FpCode;
                var ret = db.Update(s);
                return ret;
            });
        }

        /// <summary>
        /// Meta tag  güncelleme
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="setting"></param>
        public void UpdateMetaCodes(int sid, SiteSetting setting)
        {
            GetConnection(db =>
            {
                var s = SiteSettings(sid);
                s.MetaTitle = setting.MetaTitle;
                s.MetaDescription = setting.MetaDescription;
                var ret = db.Update(s);
                return ret;
            });
        }

        /// <summary>
        /// Redirection kayıt
        /// </summary>
        /// <param name="redirection"></param>
        /// <returns></returns>
        public void AddRedirect(Redirection redirection)
        {
            GetConnection(db =>
            {
                var ret = db.Insert(redirection);
                return ret;
            });
        }

        /// <summary>
        /// Redirection listeler
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Redirection> ListRedirects()
        {
            return GetConnection(db => db.GetList<Redirection>());
        }


        /// <summary>
        /// Redirection sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void DeleteRedirect(int id)
        {
            var redirection = new Redirection {Id = id};
            GetConnection(db => db.Delete(redirection));
        }

        #endregion

        #region SiteMap

        /// <summary>
        /// Site map satır getirir
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public IEnumerable<SiteMap> SiteMap()
        {
            return GetConnection(db =>
            {
                var map = db.GetList<SiteMap>();
                return map;
            });
        }

        /// <summary>
        /// Site map satır getirir
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public SiteMap SiteMapRecord(int pid, int type)
        {
            return GetConnection(db =>
            {
                var map = db.QueryFirstOrDefault<SiteMap>("SELECT * FROM SiteMap WHERE PostId=@pid AND [Type]=@type ",
                    new {pid, type});
                return map;
            });
        }

        /// <summary>
        /// Site Map data ekler
        /// </summary>
        /// <param name="sitemap"></param>
        public void AddSiteMap(SiteMap sitemap)
        {
            GetConnection(db =>
            {
                var ret = db.Insert(sitemap);
                return ret;
            });
        }

        /// <summary>
        /// Site Map  güncelleme
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="siteMap"></param>
        /// <param name="type"></param>
        public void UpdateSiteMap(int sid, int type, SiteMap siteMap)
        {
            GetConnection(db =>
            {
                var s = SiteMapRecord(sid, type);
                s.Url = siteMap.Url;
                s.Priority = siteMap.Priority;
                s.LastModified = siteMap.LastModified;
                s.ChangeFreq = siteMap.ChangeFreq;
                var ret = db.Update(s);
                return ret;
            });
        }

        /// <summary>
        /// Sitemap verilerini getirir
        /// </summary>
        /// <returns></returns>
        public IList<SiteMap> GetSiteMap()
        {
            return (IList<SiteMap>) GetConnection(db =>
            {
                var map = db.GetList<SiteMap>();
                return map;
            });
        }

        /// <summary>
        /// Sitemap güncelleme
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="val"></param>
        /// <param name="what"></param>
        public void UpdateSiteMapValue(int pid, string val, string what)
        {
            var q = $"UPDATE SiteMap SET [{what}]='{val}' WHERE PostId={pid}";
            GetConnection(db => db.Query(q));
        }

        #endregion

        public void DeleteSiteMapData(int pid, int type)
        {
            GetConnection(db =>
                db.Query("DELETE FROM SiteMap WHERE PostId=@pid AND [Type]=@type", new {pid, type})
            );
        }

        /// <summary>
        /// Special Conten listesi
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Post> GetSpecialContents()
        {
            return GetConnection(db =>
            {
                var list = db.GetList<Post>("WHERE Type=@type ORDER BY AddedDate ASC", new {PostType.SpecialContent});
                return list;
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
       
        public IEnumerable<SpecialContentElement> GetSpecialContentPosts(int contentId)
        {
            return GetConnection(db =>
            {
                var list = db.GetList<SpecialContentElement>("WHERE Type=@type ORDER BY AddedDate ASC", new {PostType.SpecialContent});
                return list;
            });
        }
    }
}
