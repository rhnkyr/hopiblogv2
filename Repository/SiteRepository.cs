﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using HopiBlog.Helpers;
using HopiBlog.Models;

namespace HopiBlog.Repository
{
    public class SiteRepository : BaseRepository
    {
        private readonly int _diff = Convert.ToInt32(Utilities.AppSetting("TimeDiff"));

        public SiteRepository(string connectionString) : base(connectionString)
        {
        }

        /// <summary>
        /// Güncel postları getirir
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        /*public IEnumerable<Post> LatestPosts(int page)
        {
            return GetConnection(db =>
                {
                    var post = db.GetListPaged<Post>(page, 9, "WHERE PostStatus=1 AND PublishDate < CAST(DATEADD(HOUR, @diff, CURRENT_TIMESTAMP)", "PublishDate DESC");
                    return post;
                }
            );
        }*/
        
        public int TotalPost()
        {
            return GetConnection(db =>
                {
                    var sql = @"SELECT Count(*) as cnt FROM Posts p WHERE p.PostStatus=1 AND p.PublishDate < CAST(DATEADD(HOUR, @diff, CURRENT_TIMESTAMP) AS DATETIME)";
                                  
                    var count = db.RecordCount<Post>(sql, new {diff = _diff});
                    return count;
                }
            );
        }
        
        
        /// <summary>
        /// Güncel postları getirir
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<object> LatestPosts(int begin, int end)
        {
            return GetConnection(db =>
                {
                    var sql = @"WITH U AS
                                    (
                                      SELECT
                                        p.PostSlug,
                                        p.PostTitle,
                                        p.PostType,
                                        p.CatSlug,
                                        c.CatName,
                                        p.MobileImage,
                                        p.PostShortDescription,
                                        ROW_NUMBER() OVER (ORDER BY p.PublishDate DESC) AS 'RowNumber'
                                        FROM Posts p
                                      JOIN Categories c
                                          ON p.CatSlug = c.CatSlug
                                      WHERE p.PostStatus=1 AND p.PublishDate < CAST(DATEADD(HOUR, @diff, CURRENT_TIMESTAMP) AS DATETIME)
                                    )
                                    SELECT
                                         *
                                    FROM U
                                    WHERE RowNumber BETWEEN @begin AND @end";

                    var list = db.Query(sql, new {begin, end, diff = _diff});
                    return list;
                }
            );
        }

        /// <summary>
        /// En çok okunanları getirir
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<object> MostRead(int count)
        {
            return GetConnection(db =>
            {
                const string sql = @"SELECT TOP (@count)
                                Posts.PostSlug,
                                Posts.PostTitle,
                                Posts.PostSlug,
                                Posts.PostType,
                                Categories.CatName,
                                Categories.CatSlug,
                                Posts.MainImage,
                                Posts.MobileImage
                            FROM
                                Posts
                            JOIN Categories
                                ON Posts.CatSlug = Categories.CatSlug
                            WHERE
                                PostStatus = 1
                            AND
                                PublishDate < CAST(DATEADD(HOUR, @diff, CURRENT_TIMESTAMP) AS DATETIME)
                            ORDER BY
                                Posts.ReadCount DESC";

                var posts = db.Query(sql, new {count, diff = _diff});

                return posts;
            });
        }

        /// <summary>
        /// Tag a göre içerikleri getirir
        /// </summary>
        /// <param name="count"></param>
        /// <param name="slug"></param>
        /// <returns></returns>
        public IList<object> GetTagContent(int count, string slug)
        {
            return GetConnection(db =>
            {
                const string sql = @"SELECT TOP (@count)
                                Posts.PostType,
                                Posts.PostSlug,
                                Posts.PostTitle,
                                Posts.PostSlug,
                                Categories.CatName,
                                Categories.CatSlug,
                                Posts.MainImage,
                                Posts.MobileImage
                            FROM
                                Posts
                            JOIN Categories
                                ON Posts.CatSlug = Categories.CatSlug
                            WHERE
                                PostStatus = 1 AND PublishDate < CAST(DATEADD(HOUR, @diff, CURRENT_TIMESTAMP) AS DATETIME) AND Posts.Id IN (
                                    SELECT
                                    PostTagReferences.PostId
                                    FROM
                                    Tags
                                    JOIN PostTagReferences
                                    ON Tags.Id = PostTagReferences.TagId
                                    WHERE
                                    TagSlug = @slug
                                )
                            ORDER BY
                                Posts.ReadCount DESC";

                var posts = db.Query(sql, new {count, slug, diff = _diff});

                return posts.ToList();
            });
        }

        /// <summary>
        /// Tag a göre içerikleri getirir
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="slug"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        /*public IList<object> GetMoreTagContent(int begin,int end, string slug)
        {
            return GetConnection(db =>
            {
                const string sql = @"SELECT TOP (@count)
                                Posts.PostType,
                                Posts.PostSlug,
                                Posts.PostTitle,
                                Posts.PostSlug,
                                Categories.CatName,
                                Categories.CatSlug,
                                Posts.MainImage,
                                Posts.MobileImage
                            FROM
                                Posts
                            JOIN Categories
                                ON Posts.CatSlug = Categories.CatSlug
                            WHERE
                                PostStatus = 1 AND PublishDate < CAST(DATEADD(HOUR, 3, CURRENT_TIMESTAMP) AS DATETIME) AND Posts.Id IN (
                                    SELECT
                                    PostTagReferences.PostId
                                    FROM
                                    Tags
                                    JOIN PostTagReferences
                                    ON Tags.Id = PostTagReferences.TagId
                                    WHERE
                                    TagSlug = @slug
                                )
                            ORDER BY
                                Posts.ReadCount DESC";

                var posts = db.Query(sql, new {count, slug});

                return posts.ToList();
            });
        }*/
        /// <summary>
        /// Kategori içeriklerini getirir
        /// </summary>
        /// <param name="count"></param>
        /// <param name="slug"></param>
        /// <returns></returns>
        public IEnumerable<object> GetCategoryContent(int count, string slug)
        {
            return GetConnection(db =>
            {
                const string sql = @"SELECT TOP (@count)
                                Posts.PostSlug,
                                Posts.PostTitle,
                                Posts.PostSlug,
                                Posts.PostType,
                                Posts.PostShortDescription,
                                Categories.CatName,
                                Categories.CatSlug,
                                Posts.MainImage,
                                 Posts.MobileImage
                            FROM
                                Posts
                            JOIN Categories
                                ON Posts.CatSlug = Categories.CatSlug
                            WHERE
                                PostStatus = 1 AND Posts.CatSlug = @slug AND PublishDate < CAST(DATEADD(HOUR, @diff,     CURRENT_TIMESTAMP) AS DATETIME)
                            ORDER BY
                                Posts.PostTitle";

                var posts = db.Query(sql, new {count, slug, diff = _diff});

                return posts;
            });
        }

        /// <summary>
        /// Post sayısını getirir
        /// </summary>
        /// <returns></returns>
        public int CountPosts()
        {
            return GetConnection(db =>
            {
                var posts = db.RecordCount<Post>();
                return posts;
            });
        }

        /// <summary>
        /// Arama sonuçları getirir
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public IEnumerable<object> GetSearchResults(string word)
        {
            return GetConnection(db =>
            {
                const string sql = @"SELECT
                                Posts.PostSlug,
                                Posts.PostTitle,
                                Posts.PostSlug,
                                Posts.PostType,
                                Posts.PostShortDescription,
                                Categories.CatName,
                                Categories.CatSlug,
                                Posts.MainImage
                            FROM
                                Posts
                            JOIN Categories
                                ON Posts.CatSlug = Categories.CatSlug
                            WHERE
                                PostStatus = 1 AND PublishDate < CAST(DATEADD(HOUR, @diff, CURRENT_TIMESTAMP) AS DATETIME) AND Posts.PostTitle LIKE @lQuery OR Posts.PostShortDescription LIKE @lQuery
                            ORDER BY
                                Posts.PostTitle";

                var posts = db.Query(sql, new {diff = _diff, lQuery = "%" + word + "%"});

                return posts;
            });
        }

        /// <summary>
        /// Tag adını getirir getirir
        /// </summary>
        /// <param name="slug">Post slug</param>
        /// <returns></returns>
        public string GetTagName(string slug)
        {
            return GetConnection(db =>
            {
                var tag = db.QueryFirstOrDefault<Tag>("SELECT TagName FROM Tags WHERE TagSlug=@slug",
                    new {slug});
                return tag.TagName;
            });
        }

        /// <summary>
        /// Category getirir getirir
        /// </summary>
        /// <param name="slug">Category slug</param>
        /// <returns></returns>
        public string GetPostCategory(string slug)
        {
            return GetConnection(db =>
            {
                var category = db.QueryFirstOrDefault<Category>("SELECT CatName FROM Categories WHERE CatSlug=@slug",
                    new {slug});
                return category.CatName;
            });
        }

        /// <summary>
        /// Kategory adını getirir
        /// </summary>
        /// <param name="slug">Post slug</param>
        /// <returns></returns>
        public string GetCategoryName(string slug)
        {
            return GetConnection(db =>
            {
                var cat = db.QueryFirstOrDefault<Category>(
                    "SELECT CatName FROM Categories WHERE CatSlug=@slug",
                    new {slug});
                return cat.CatName;
            });
        }

        /// <summary>
        /// Sliderları getirir
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Slider> Sliders()
        {
            return GetConnection(db =>
            {
                var sliders = db.GetList<Slider>("ORDER BY Ord ASC");
                return sliders;
            });
        }

        /// <summary>
        /// Post içeriğini getirir
        /// </summary>
        /// <param name="slug">Post slug</param>
        /// <returns></returns>
        public Post GetPost(string slug)
        {
            return GetConnection(db =>
            {
                var post = db.QueryFirstOrDefault<Post>("SELECT * FROM Posts WHERE PostSlug=@slug", new {slug});
                return post;
            });
        }

        /// <summary>
        /// Galeri postu için görselleri getirir
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public IList<PostImage> GetPostImages(int postId)
        {
            return (IList<PostImage>) GetConnection(db =>
            {
                var images = db.GetList<PostImage>("WHERE PostId=@postId", new {postId});
                return images;
            });
        }

        /// <summary>
        /// Listelerin elementlerini getirir
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public IEnumerable<ListElement> GetListElements(int postId)
        {
            return GetConnection(db =>
            {
                var total = db.GetList<ListElement>("WHERE PostId=@postId", new {postId});
                return total;
            });
        }

        /// <summary>
        /// Kullanıcı Kayıt yardımcı
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Subscription CheckUserSubscription(string email)
        {
            return GetConnection(db =>
                db.QueryFirstOrDefault<Subscription>("SELECT Id FROM Subscriptions WHERE Email = @email",
                    new {email})
            );
        }

        /// <summary>
        /// Subscription ekler
        /// </summary>
        /// <param name="sub"></param>
        /// <returns></returns>
        public int? SaveEmail(Subscription sub)
        {
            return GetConnection(db =>
            {
                var total = db.Insert(sub);
                return total;
            });
        }


        /// <summary>
        /// Post taglerini getirir
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public IList<Tag> GetPostTags(int postId)
        {
            return (IList<Tag>) GetConnection(db =>
            {
                const string sql = @"SELECT
                        Tags.Id,
                        Tags.TagName,
                        Tags.TagSlug
                            FROM
                        dbo.Posts
                            JOIN PostTagReferences
                            ON Posts.Id = PostTagReferences.PostId
                        JOIN dbo.Tags
                            ON PostTagReferences.TagId = Tags.Id
                        WHERE
                        Posts.Id = @postId
                        ORDER BY
                        dbo.Tags.TagName ASC";

                var tags = db.Query<Tag>(sql, new {postId});
                return tags;
            });
        }

        /// <summary>
        /// Benzer postları listesi
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        /*public IList<Post> GetRelatedPosts(int postId)
        {
            return (IList<Post>) GetConnection(db =>
            {
                   var posts = db.Query<Post>(@"SELECT * FROM Posts WHERE Id IN (SELECT
                                                            Tags.Id,
                                                            Tags.TagName,
                                                            Tags.TagSlug
                                                                FROM
                                                            dbo.Posts
                                                                JOIN PostTagReferences
                                                                ON Posts.Id = PostTagReferences.PostId
                                                            JOIN dbo.Tags
                                                                ON PostTagReferences.TagId = Tags.Id
                                                            WHERE
                                                            Posts.Id = @postId
                                                            ORDER BY
                                                            dbo.Tags.TagName ASC)", new {postId});
                return posts;
            });
        }*/
        /// <summary>
        /// Benzer postları listesi
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="catSlug"></param>
        /// <returns></returns>
        public IList<Post> GetRelatedPosts(int postId, string catSlug)
        {
            return (IList<Post>) GetConnection(db =>
            {
                var posts = db.Query<Post>(@"SELECT p.PostTitle, p.ThumbImage, p.ImageAltText,p.PostSlug
                                             FROM Posts p WHERE p.Id
                                             IN (SELECT TOP 5 PERCENT Id FROM Posts ORDER BY newid())
                                             AND p.Id <> 9 AND p.CatSlug = @catSlug AND p.PublishDate < CAST(DATEADD(HOUR, @diff, CURRENT_TIMESTAMP) AS DATETIME)",
                    new {postId, catSlug,diff = _diff});
                return posts;
            });
        }

        #region Ajax

        /*public async Task<IEnumerable<User>> GetUsers(int begin, int end)
        {
            return await WithConnection(async db =>
            {
                var sql = @"WITH U AS
                                    (
                                      SELECT Id, FirstName, LastName, Email, Phone, CreatedDate,
                                        ROW_NUMBER() OVER (ORDER BY u.CreatedDate DESC) AS 'RowNumber'
                                        FROM Users u
                                    )
                                    SELECT *
                                    FROM U
                                    WHERE RowNumber BETWEEN @begin AND @end";

                var list = await db.QueryAsync<User>(sql, new {begin, end});
                return list;
            });
        }*/

        #endregion

        #region Categories

        /// <summary>
        /// Categorileri getirir
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Category> GetCategories(int type)
        {
            return GetConnection(db =>
                db.Query<Category>("SELECT * FROM Categories Where ShowOtherTab = @type", new {type}).ToList());
        }

        /// <summary>
        /// Categorileri getirir
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Category> GetCategories()
        {
            return GetConnection(db =>
                db.Query<Category>("SELECT * FROM Categories").ToList());
        }

        #endregion

        /// <summary>
        /// Site Ayarlarını getirir
        /// </summary>
        /// <returns></returns>
        public SiteSetting Settings()
        {
            return GetConnection(db =>
            {
                var settings = db.QueryFirstOrDefault<SiteSetting>("SELECT * FROM SiteSettings WHERE Id = 1");
                return settings;
            });
        }

        /*public IEnumerable<ResultObject> GetLotsOfData(Func<IEnumerable<MyMapObject>, IEnumerable<ResultObject>> process)
        {
            return GetConnection(c => c.Query<MyMapObject>(query, buffered: false), process);
        }*/

        /// <summary>
        /// Sitemap verilerini getirir
        /// </summary>
        /// <returns></returns>
        public IList<SiteMap> GetSiteMap()
        {
            return (IList<SiteMap>) GetConnection(db =>
            {
                var map = db.GetList<SiteMap>();
                return map;
            });
        }
    }
}
