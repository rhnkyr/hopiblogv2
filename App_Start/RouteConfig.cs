﻿using System.Web.Mvc;
using System.Web.Routing;
using HopiBlog.Helpers;

namespace HopiBlog
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
     

            var prefix = Utilities.AppSetting("RoutePrefix") == "" ? "" : Utilities.AppSetting("RoutePrefix")+"/";
            
            routes.MapRoute(
                "AdminLogin",
                prefix + "yonetim",
                new {controller = "AdminHome", action = "Index"}
            );

            routes.MapRoute(
                "Backoffice",
                prefix + "Backoffice/{action}/{id}",
                new {controller = "Backoffice", action = "Index", id = UrlParameter.Optional}
            );

            routes.MapRoute(
                "Sitemap",
                prefix + "sitemap",
                new {controller = "Home", action = "SiteMap"}
            );

            routes.MapRoute(
                "LoadMore",
                prefix + "load-more",
                new {controller = "Home", action = "LoadMore"}
            );
            
            routes.MapRoute(
                "LoadMoreTags",
                prefix + "load-more-tags",
                new {controller = "Home", action = "LoadMoreTags"}
            );

            routes.MapRoute(
                "Search",
                prefix + "arama",
                new {controller = "Home", action = "Search"}
            );

            routes.MapRoute(
                "ShowPhotoContent",
                prefix + "fotograf-galerisi/{category}/{slug}",
                new
                {
                    controller = "Home",
                    action = "ShowContent"
                }
            );

            routes.MapRoute(
                "ShowVideoContent",
                prefix + "video/{category}/{slug}",
                new
                {
                    controller = "Home",
                    action = "ShowContent"
                }
            );


            routes.MapRoute(
                "ShowListContent",
                prefix + "liste/{category}/{slug}",
                new {controller = "Home", action = "ShowContent"}
            );

            routes.MapRoute(
                "ShowTagContent",
                prefix + "etiket/{slug}",
                new {controller = "Home", action = "ShowTagContent", slug = UrlParameter.Optional}
            );

            routes.MapRoute(
                "ShowCategoryContent",
                prefix + "kategori/{slug}",
                new {controller = "Home", action = "ShowCategoryContent"}
            );

            routes.MapRoute(
                "ShowShoppingContent",
                prefix + "alisveris/{slug}",
                new {controller = "Home", action = "ShowContent"}
            );
            
            routes.MapRoute(
                "SaveEmail",
                prefix + "abonelik-kayit",
                new {controller = "Home", action = "SaveEmail"}
            );

            routes.MapRoute(
                "ShowPlainAndSpecialContent",
                prefix + "{slug}",
                new {controller = "Home", action = "ShowContent"}
            );

            /*routes.MapRoute(
                "Preview",
                "{slug}/on-izleme",
                new {controller = "Home", action = "Preview"}
            );*/


            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                new {controller = "Home|Backoffice|AdminHome|Error"}
            );

            //catchAll routes to grab the URLs we want to redirect

            routes.MapRoute(
                "Redirect_OrError",
                "{*CatchAll1*}",
                new {controller = "Error", action = "NotFound"}
            );

            routes.MapRoute(
                "Redirect_OrError_two_levels",
                "{CatchAll1*}/{CatchAll2*}",
                new {controller = "Error", action = "NotFound"}
            );

            routes.MapRoute(
                "Redirect_OrError_three_levels",
                "{CatchAll1*}/{CatchAll2*}/{CatchAll3*}",
                new {controller = "Error", action = "NotFound"}
            );
            routes.MapRoute(
                "Redirect_OrError_four_levels",
                "{CatchAll1*}/{CatchAll2*}/{CatchAll3*}/{CatchAll4*}",
                new {controller = "Error", action = "NotFound"}
            );
        }
    }
}
