﻿using System.Web.Optimization;
using HopiBlog.Helpers;

namespace HopiBlog
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            var prefix = Utilities.AppSetting("RoutePrefix") == "" ? "" : Utilities.AppSetting("RoutePrefix") + "/";

            bundles.Add(new ScriptBundle("~/"+prefix+"bundles/libs").Include(
                "~/" + prefix + "Static/fe/assets/js/jquery.js",
                "~/" + prefix + "Static/fe/assets/js/jquery.easing.min.js",
                "~/" + prefix + "Static/fe/assets/js/jquery.maskedinput.min.js",
                "~/" + prefix + "Static/fe/assets/js/owl.carousel.min.js",
                "~/" + prefix + "Static/fe/assets/js/jquery.fancybox.pack.js",
                "~/" + prefix + "Static/fe/assets/js/swiper.jquery.min.js"
            ));

            bundles.Add(new ScriptBundle("~/"+prefix+"bundles/site").Include(
                "~/" + prefix + "Static/fe/assets/js/hopi-blog.js",
                "~/" + prefix + "Static/fe/assets/js/hopi-blog-special.js"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add(new StyleBundle("~/"+prefix+"bundles/css").Include(
                "~/" + prefix + "Static/fe/assets/css/animate.css",
                "~/" + prefix + "Static/fe/assets/css/swiper.min.css",
                "~/" + prefix + "Static/fe/assets/css/hb-main.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
