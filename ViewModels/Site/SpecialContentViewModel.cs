﻿using System.Collections.Generic;
using HopiBlog.Models;

namespace HopiBlog.ViewModels.Site
{
    public class SpecialContentViewModel
    {
        public IEnumerable<ListElement> ListElements { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public Post Post { get; set; }
        public Post SubPost { get; set; }
        public bool IsMobile { get; set; }
        public IList<Post> RelatedPosts { get; set; }
    }
}
