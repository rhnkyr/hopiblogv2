﻿using System.Collections.Generic;
using HopiBlog.Models;

namespace HopiBlog.ViewModels.Site
{
    public class HomePageViewModel
    {
        public IEnumerable<Slider> Sliders { get; set; }
        public IEnumerable<object> LatestPosts { get; set; }
        public IEnumerable<object> MostRead { get; set; }
        public int TotalPost { get; set; }
    }
}
