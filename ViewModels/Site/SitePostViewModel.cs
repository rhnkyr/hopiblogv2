﻿using System.Collections.Generic;
using HopiBlog.Models;

namespace HopiBlog.ViewModels.Site
{
    public class SitePostViewModel
    {
        public IList<Tag> Tags { get; set; }
        public Post Post { get; set; }
        public string CategoryName { get; set; }
        public IList<Post> RelatedPosts { get; set; }
        public IList<PostImage> PostImages { get; set; }
    }
}
