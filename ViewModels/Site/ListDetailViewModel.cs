﻿using System.Collections.Generic;
using HopiBlog.Models;

namespace HopiBlog.ViewModels.Site
{
    public class ListDetailViewsModel
    {
        public IEnumerable<ListElement> ListElements { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public Post Post { get; set; }
        public string CategoryName { get; set; }
        public IList<Post> RelatedPosts { get; set; }
    }
}
