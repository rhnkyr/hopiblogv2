﻿using System.Collections.Generic;

namespace HopiBlog.ViewModels.Site
{
    public class ContentListViewModel
    {
        public IEnumerable<object> Post { get; set; }
        public string Tag { get; set; }
        public int Count { get; set; }
        public string Category { get; set; }
    }
}
