﻿using System.Collections.Generic;
using HopiBlog.Models;

namespace HopiBlog.ViewModels.Admin
{
    public class PostDetailViewModel
    {
        public Post Post { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<ListElement> ListElements { get; set; }
        public IEnumerable<PostImage> PostImages { get; set; }
    }
}
