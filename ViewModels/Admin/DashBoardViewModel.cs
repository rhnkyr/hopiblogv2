﻿using System.Collections.Generic;
using HopiBlog.Models;

namespace HopiBlog.ViewModels.Admin
{
    public class DashBoardViewModel
    {
        public IEnumerable<Post> Posts  { get; set; }
    }
}
