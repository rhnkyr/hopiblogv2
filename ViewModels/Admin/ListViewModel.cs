﻿using System.Collections.Generic;
using HopiBlog.Models;

namespace HopiBlog.ViewModels.Admin
{
    public class ListViewModel
    {
        public IEnumerable<Post> Posts { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalItemCount { get; set; }
    }
}
