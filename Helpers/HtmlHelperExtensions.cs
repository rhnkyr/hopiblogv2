﻿using System.Web.Mvc;

namespace HopiBlog.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString SetUrl(this HtmlHelper htmlHelper, byte type, string slg, string cat = null)
        {
            var url = new UrlHelper(htmlHelper.ViewContext.RequestContext);
           
            MvcHtmlString link;
            
            switch (type)
            {
                case 1:
                    link = new MvcHtmlString(url.RouteUrl("ShowPlainAndSpecialContent", new {slug = slg}));
                    break;
                case 2:
                    link = new MvcHtmlString(url.RouteUrl("ShowVideoContent", new { category = cat, slug = slg}));
                    break;
                case 3:
                    link = new MvcHtmlString(url.RouteUrl("ShowPhotoContent", new {category = cat, slug = slg}));
                    break;
                case 4:
                    link = new MvcHtmlString(url.RouteUrl("ShowListContent", new {category = cat, slug = slg}));
                    break;
                case 5:
                    link = new MvcHtmlString(url.RouteUrl("ShowShoppingContent", new { slug = slg}));
                    break;
                default:
                    link = new MvcHtmlString("/");
                    break;
            }

            return link;
        }
    }
}
