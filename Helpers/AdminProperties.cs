﻿using System.Web;
using HopiBlog.Models;

namespace HopiBlog.Helpers
{
    public class AdminProperties
    {
        public static BackEndUser CurrentMember
        {
            get
            {
                if (HttpContext.Current.Session["Admin"] != null)
                    return HttpContext.Current.Session["Admin"] as BackEndUser;

                return null;
            }
            set { HttpContext.Current.Session["Admin"] = value; }
        }
    }
}
