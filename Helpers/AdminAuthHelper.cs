﻿using System.Web.Mvc;

namespace HopiBlog.Helpers
{
    public class AdminAuthHelper
    {
        public class RoleAuthorization : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                var isAuthanticated = AdminProperties.CurrentMember != null;
                if (isAuthanticated) return;
                var bundle = Utilities.AppSetting("RoutePrefix") == ""
                    ? "~"
                    : "/" + Utilities.AppSetting("RoutePrefix");
                filterContext.HttpContext.Response.Redirect(bundle + "/yonetim");
            }
        }
    }
}
